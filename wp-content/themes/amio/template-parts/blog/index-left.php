<?php $amio_options = get_option('amio'); ?> 
<div class="row">
	<div class="col-md-8 pull-right">
								<?php global $post, $post_id;?>
							   <?php while ( have_posts() ) : the_post();?>
							<!-- Blog Post -->
								<div class="blog-post">
								<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
								
									<?php if( has_post_format( 'video' ) !='') :?>
									<?php get_template_part('template-parts/blog/post-type/video');?>
									<?php elseif( has_post_format( 'gallery' ) !='') :?>
									<?php get_template_part('template-parts/blog/post-type/gallery');?>
									<?php elseif( has_post_format( 'image' ) !='') :?>
									<?php get_template_part('template-parts/blog/post-type/image');?>
									<?php elseif( has_post_format( 'audio' ) !='') :?>
									<?php get_template_part('template-parts/blog/post-type/audio');?>
									<?php else:?>
									<?php endif;?>
									
									<!-- Post Title -->
									<h3 class="t-left f-medium dark ultrabold uppercase"><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
									
									<!-- Post Information -->
									<ul class="t-left lato f-small">
										<li class="no-margin no-border no-padding"><?php if(!empty($amio_options['wr-author'])):?>
										<?php echo esc_attr(Amio_AfterSetupTheme::return_thme_option('wr-author',''));?>
										<?php else:?>
										<?php esc_attr_e('Author: ','amio');?>
										<?php endif;?><a href="<?php the_permalink();?>"><?php the_author();?></a></li>
										<li><?php if(!empty($amio_options['wr-date'])):?>
										<?php echo esc_attr(Amio_AfterSetupTheme::return_thme_option('wr-date',''));?>
										<?php else:?>
										<?php esc_attr_e('Date: ','amio');?>
										<?php endif;?><a href="<?php the_permalink();?>"><?php the_time( get_option( 'date_format' ) ); ?></a></li>
										<li><?php if(!empty($amio_options['wr-in'])):?>
										<?php echo esc_attr(Amio_AfterSetupTheme::return_thme_option('wr-in',''));?>
										<?php else:?>
										<?php esc_attr_e('In: ','amio');?>
										<?php endif;?><?php the_category(', ');?></li>
										<?php
										if( has_tag() ) {?>
										<li><?php the_tags();?></li>
										<?php }?>
									</ul>
									<!-- End Post Information -->
									
									
									<!-- Post Snippet -->
									<p class="t-left">
										<?php
										$amio_excerpt= substr(strip_tags($post->post_content), 0, 183);
										update_post_meta(get_the_ID(), 'amio_excerpt', $amio_excerpt);
										echo esc_html($amio_excerpt);
										?>
									</p>
									<!-- End Post Snippet -->
									
									<!-- Button -->
									<a href="<?php the_permalink();?>" class="button float-l">
									<?php if(!empty($amio_options['wr-rdmore'])):?>
									<?php echo esc_attr(Amio_AfterSetupTheme::return_thme_option('wr-rdmore',''));?>
									<?php else:?>
									<?php esc_attr_e('Read More','amio');?>
									<?php endif;?>
									</a>
									
								</div>
								</div>
								<!-- End Blog Post -->
								<?php endwhile; ?>
								<?php wp_reset_postdata();?>
								<?php if (function_exists("amio_pagination")) { ?>
								<?php amio_pagination($wp_query->max_num_pages);
								?>
								<?php } ?>
	</div>
	<div class="col-md-4 pull-left">
		<?php
		if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Blog Sidebar')): 
		endif;
		?>
	</div>
</div>