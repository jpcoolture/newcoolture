<?php
/**
 * Registering meta boxes
 *
 * All the definitions of meta boxes are listed below with comments.
 * Please read them CAREFULLY.
 *
 * You also should read the changelog to know what has been changed before updating.
 *
 * For more information, please visit:
 * @link http://www.deluxeblogtips.com/meta-box/docs/define-meta-boxes
 */

/********************* META BOX DEFINITIONS ***********************/

/**
 * Prefix of meta keys (optional)
 * Use underscore (_) at the beginning to make keys hidden
 * Alt.: You also can make prefix empty to disable it
 */
// Better has an underscore as last sign
$prefix = 'rnr_';

global $meta_boxes;

$meta_boxes = array();

global $smof_data;


/* ----------------------------------------------------- */
// Page Sections Metaboxes
/* ----------------------------------------------------- */


/* ----------------------------------------------------- */
// Revolution Slider
/* ----------------------------------------------------- */

$revolutionslider = array();
$revolutionslider[0] = 'No Slider';

if(class_exists('RevSlider')){
    $slider = new RevSlider();
	$arrSliders = $slider->getArrSliders();
	foreach($arrSliders as $revSlider) { 
		$revolutionslider[$revSlider->getAlias()] = $revSlider->getTitle();
	}
}

/* Page Section Background Settings */

$grid_array = array('2 Columns','3 Columns','4 Columns');

$pagebg_type_array = array(
	'image' => 'Image',
	'gradient' => 'Gradient',
	'color' => 'Color'
);



/* ----------------------------------------------------- */
/* page Type Metaboxes
/* ----------------------------------------------------- */
$meta_boxes[] = array(
	'id' => 'page_type',
	'title' => 'Default Page Function',
	'pages' => array( 'page' ),
	'context' => 'normal',	

	'fields' => array(
		
		// SELECT BOX
		array(
			'name'     => __( 'Select', 'cinestar' ),
			'id'   => $prefix . 'wr-pagetype',
			'desc'  => __( '', 'amio' ),
			'type'     => 'image_select',
			// Array of 'value' => 'Label' pairs for select box
			'options'  => array(
				'st1' => __( get_template_directory_uri().'/includes/metaboxes/img/wr-page-default.png', 'amio' ),
				'st2' => __( get_template_directory_uri().'/includes/metaboxes/img/page-one.png', 'amio' ),
				
				
				
			),
			'desc'  => __( '<br><br><br><br><br><br>Select Default Page Function as Amio VC Element if you want to use VC Element From Amio Ctaegory, Page Header & Portfolio Element.', 'amio' ),
			// Select multiple values, optional. Default is false.
			'multiple'    => false,
			'std'         => 'st1',
			'placeholder' => __( 'Select an Option', 'amio' ),
		),
		
		array(
			'name'		=> 'Page Sub Title',
			'id'		=> $prefix . 'pagesubtt',
			'clone'		=> false,
			'type'		=> 'text',
			'std'		=> '',
			'desc'		=> 'Working on all page templates.',
		),

		
	
	)
);



/* ----------------------------------------------------- */
/* page Type Metaboxes
/* ----------------------------------------------------- */
$meta_boxes[] = array(
	'id' => 'blog_page_type',
	'title' => 'Blog Page Template Function',
	'pages' => array( 'page' ),
	'context' => 'normal',	

	'fields' => array(
		
		// SELECT BOX
		array(
			'name'     => esc_attr__( 'Blog Layout', 'amio' ),
			'id'   => $prefix . 'wrblog-pagetype',
			'desc'  => __( 'Working only Blog Page Template', 'amio' ),
			'type'     => 'image_select',
			// Array of 'value' => 'Label' pairs for select box
			'options'  => array(
				'st1' => esc_attr__( get_template_directory_uri().'/includes/metaboxes/img/wr-page-default.png', 'amio' ),
				'st2' => esc_attr__( get_template_directory_uri().'/includes/metaboxes/img/wrfixedpage.png', 'amio' ),
				'st3' => esc_attr__( get_template_directory_uri().'/includes/metaboxes/img/wr-page-left.png', 'amio' ),
				'st4' => esc_attr__( get_template_directory_uri().'/includes/metaboxes/img/wr-page-right.png', 'amio' ),
				
				
			),
			'desc'  => esc_attr__( '', 'amio' ),
			// Select multiple values, optional. Default is false.
			'multiple'    => false,
			'std'         => 'st3',
			'placeholder' => __( 'Select an Option', 'amio' ),
		),
		array(
				'name'       => esc_attr__( 'Number Of Post Show', 'blps' ),
				'id'         => $prefix . 'blog-post-show',
				'desc'		=> '',
				'type'       => 'slider',
				// Text labels displayed before and after value
				'prefix'     => __( '', 'blps' ),
				'suffix'     => __( ' Posts', 'blps' ),
				'js_options' => array(
					'min'  => 1,
					'max'  => 100,
					'step' => 1,
				),
			),	

			array(
			'name'		=> 'Exclude Category',
			'id'		=> $prefix . 'blog-post-cat',
			'desc'		=> 'Enter category name ex: web design, web development (Optional).',
			'clone'		=> false,
			'type'		=> 'text',
			'std'		=> ''
		),

			
	)
);


/* ----------------------------------------------------- */
/* Post  Type Portfolio Metaboxes
/* ----------------------------------------------------- */
$meta_boxes[] = array(
	'id' => 'port_click_type',
	'title' => 'Portfolio Item Click Option',
	'pages' => array( 'portfolio' ),
	'context' => 'normal',	

	'fields' => array(
		
		array(
			'name'     => __( 'Click Action', 'amio' ),
			'id'   => $prefix . 'port_click_opt',
			'type'     => 'select',
			// Array of 'value' => 'Label' pairs for select box
			'options'  => array(
				'st1' => esc_attr__( 'Go To Details Page', 'amio' ),
				'st2' => esc_attr__( 'Popup', 'amio' ),
				
			),
			'desc'		=> '',
			// Select multiple values, optional. Default is false.
			'std'         => 'st1',

		),
		
		
		array(
			'name'		=> 'Sub Title',
			'id'		=> $prefix . 'portsubttt',
			'clone'		=> false,
			'type'		=> 'text',
			'std'		=> '',
			'desc'		=> '',
		),

		
	
	)
);


// Blog Post Metaboxes
/* ----------------------------------------------------- */
$meta_boxes[] = array(
	'id' => 'rnr-blogmeta-subtitle',
	'title' => 'Sub Title',
	'pages' => array( 'post'),
	'context' => 'normal',

	// List of meta fields
	'fields' => array(

		array(
			'name'		=> 'Sub Title',
			'id'		=> $prefix . 'bl_sub_title',
			'desc'		=> '',
			'clone'		=> false,
			'type'		=> 'text',
			'std'		=> ''
		),

		
	)
);

$meta_boxes[] = array(
	'id' => 'rnr-blogmeta-video',
	'title' => 'Post Format Video Option',
	'pages' => array( 'post'),
	'context' => 'normal',

	// List of meta fields
	'fields' => array(

		array(
			'name'		=> 'Vimeo/ Youtube Video Link:',
			'id'		=> $prefix . 'bl-video',
			'desc'		=> 'Set Vimeo / Youtube Video Embed Link',
			'clone'		=> false,
			'type'		=> 'text',
			'std'		=> ''
		),

		
	)
);

// Blog Post Metaboxes
/* ----------------------------------------------------- */


$meta_boxes[] = array(
	'id' => 'rnr-blogmeta-audio',
	'title' => 'Post Format Audio Option',
	'pages' => array( 'post'),
	'context' => 'normal',

	// List of meta fields
	'fields' => array(

		array(
			'name'		=> 'Soundcloud Audio Link:',
			'id'		=> $prefix . 'bl-audio',
			'desc'		=> '',
			'clone'		=> false,
			'type'		=> 'text',
			'std'		=> ''
		),

		
	)
);

/* ----------------------------------------------------- */
/* page Type Metaboxes
/* ----------------------------------------------------- */
$meta_boxes[] = array(
	'id' => 'blog_postgaleery_type',
	'title' => 'Post Format Gallery Option',
	'pages' => array( 'post' ),
	'context' => 'normal',	

	'fields' => array(
		
		array(
			'name'		=> 'Upload Image',
			'id'		=> $prefix . 'wr_slide_show_blog',
			'clone'		=> false,
			'type'		=> 'image_advanced',
			'desc'		=> 'Select Post Format <b>Gallery</b>.<br> Select Minimum two Images.',
		),	
		
	
		
	)
);

/* ----------------------------------------------------- */
/* portfolio Post Type Metaboxes
/* ----------------------------------------------------- */
$meta_boxes[] = array(
	'id' => 'portfolio_type',
	'title' => 'Portfolio Foramt',
	'pages' => array( 'portfolio' ),
	'context' => 'normal',	

	'fields' => array(
		
		// SELECT BOX
		array(
			'name'     => esc_attr__( 'Select', 'amio' ),
			'id'   => $prefix . 'wr_portdt_pagetype',
			'desc'  => esc_attr__( '', 'amio' ),
			'type'     => 'image_select',
			// Array of 'value' => 'Label' pairs for select box
			'options'  => array(
				'st1' => __( get_template_directory_uri().'/includes/metaboxes/img/wr-page-default.png', 'amio' ),
				'st3' => __( get_template_directory_uri().'/includes/metaboxes/img/image-gallery.png', 'amio' ),
				'st2' => __( get_template_directory_uri().'/includes/metaboxes/img/home-video.png', 'amio' ),
				'st4' => __( get_template_directory_uri().'/includes/metaboxes/img/image-gallery2.png', 'amio' ),
				
			),
			// Select multiple values, optional. Default is false.
			'multiple'    => false,
			'std'         => 'st1',
			'placeholder' => esc_attr__( 'Select an Option', 'amio' ),
		),
		
		array(
			'name'		=> 'Sub Title',
			'id'		=> $prefix . 'port-sub-tits',
			'desc'		=> '',
			'clone'		=> false,
			'type'		=> 'textarea',
			'std'		=> ''
		),

		
		
		
		
		
	)
);

// Blog Post Metaboxes
/* ----------------------------------------------------- */


$meta_boxes[] = array(
	'id' => 'rnr-portmeta-video',
	'title' => 'Portfolio Format Video Option',
	'pages' => array( 'portfolio'),
	'context' => 'normal',

	// List of meta fields
	'fields' => array(

		array(
			'name'		=> 'Vimeo/ Youtube Video Link:',
			'id'		=> $prefix . 'bl-video',
			'desc'		=> 'Set Vimeo / Youtube Video Embed Link',
			'clone'		=> false,
			'type'		=> 'text',
			'std'		=> ''
		),

		
	)
);

/* portfolio Post Type Metaboxes
/* ----------------------------------------------------- */
$meta_boxes[] = array(
	'id' => 'pt_slider_link',
	'title' => 'Image Gallery Option(Working only Portfolio Default Style.)',
	'pages' => array( 'portfolio' ),
	'context' => 'normal',	

	'fields' => array(
		
		array(
			'name'		=> 'Image Gallery',
			'id'		=> $prefix . 'portfolio-image',
			'clone'		=> false,
			'type'		=> 'image_advanced',
			'desc'		=> 'Upload Images ',
		),	
		
		
		
			
		
		
	)
);


/********************* META BOX REGISTERING ***********************/

/**
 * Register meta boxes
 *
 * @return void
 */
function rocknrolla_register_meta_boxes()
{
	global $meta_boxes;

	// Make sure there's no errors when the plugin is deactivated or during upgrade
	if ( class_exists( 'RW_Meta_Box' ) )
	{
		foreach ( $meta_boxes as $meta_box )
		{
			new RW_Meta_Box( $meta_box );
		}
	}
}

// Hook to 'admin_init' to make sure the meta box class is loaded before
// (in case using the meta box class in another plugin)
// This is also helpful for some conditionals like checking page template, categories, etc.
add_action( 'admin_init', 'rocknrolla_register_meta_boxes' );