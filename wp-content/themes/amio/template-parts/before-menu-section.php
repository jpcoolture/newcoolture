<?php defined('ABSPATH') or die("No script kiddies please!");?>
<?php $amio_options = get_option('amio'); ?>
<!-- Sticky Navigation -->
				<div data-uk-sticky>
				
					<!-- Navigation Starts -->
					<div class="navigation">
						
						
						
						<!-- Navigation Heading -->
						<div class="navbar-header">
							
							<!-- Mobile Navigation Button -->
							<button type="button" class="amio-navbar-toggle">
								<span class="sr-only"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<!-- End Mobile Navigation Button -->
							
							<!-- Navigation Logo -->
							<?php if (Amio_AfterSetupTheme::return_thme_option('wr-logo-main')=='st2'){?>
							<a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url(Amio_AfterSetupTheme::return_thme_option('logopic','url'));?>" alt="" /></a>
							<?php } else{ ?>
							<?php if(!empty($amio_options['wrlogotxt'])):?>
							<a class="navbar-brand text-logo-wr" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php echo esc_attr($amio_options['wrlogotxt']);?></a>
							<?php else:?>
							<a class="navbar-brand text-logo-wr" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php esc_attr_e('Amio','amio');?></a>
							<?php endif;?>
							<?php } ?>
							<!-- End Navigation Logo -->
							
						</div>
						<!-- End Navigation Heading -->
						

						
						
						<!-- Navigation Links -->
						<div class="amio-collapse amio-navbar-collapse" id="amio-mega">
						
							