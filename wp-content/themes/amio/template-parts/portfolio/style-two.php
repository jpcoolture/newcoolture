<?php $amio_options = get_option('amio'); ?> 
<!-- Row -->
						<div class="row">
						
							<!-- Column -->
							<div class="col-md-12">
							
								<!-- Video -->
								<?php if (get_post_meta($post->ID,'rnr_bl-video',true)!=''):?>
							<!-- Begin Vimeo embed video -->
							<iframe src="<?php echo esc_url(get_post_meta($post->ID,'rnr_bl-video',true));?>" class="videos wr-videos-new" title="Vimeo Example" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>	
									
								
							<!-- End Vimeo embed video -->
							
							<?php endif;?>
								<!-- End Video -->
								
							</div>
							<!-- End Column -->
							
						</div>
						<!-- End Row -->
		<!-- Row -->
		<div class="row">
			<!-- Column -->
			<div class="col-md-12 t-left brief">
				<?php if(have_posts()) : while ( have_posts() ) : the_post();?>
				<?php the_content();?>
				<?php endwhile;  endif; wp_reset_postdata(); ?>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->