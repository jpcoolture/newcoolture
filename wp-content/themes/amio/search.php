<?php $amio_options = get_option('amio'); ?> 
<?php
get_header();
 ?>
<?php if (class_exists('WooCommerce')) { ?>
<ul class="nav navbar-nav f-small woo-nab-ico normal raleway"><li><a href="<?php echo $woocommerce->cart->get_cart_url(); ?>"><i class="ion-bag f-normal"></i></a></li></ul>
<?php } ?>
<?php get_template_part('template-parts/after-menu-section');?>
 <!-- Header -->
				<div id="header">
				
					<!-- Section Content -->
					<div class="section-content">
				
						
						<!-- Heading -->
						<div class="big-text" data-uk-scrollspy="{cls:'uk-animation-fade', delay:20}">
						
							<!-- Title -->
							<h2 class="t-left f-large ultrabold uppercase">
							<?php esc_attr_e('Search','amio');?>
							
							</h2>
							
							<!-- Description -->
							<h3 class="t-left f-medium normal uppercase"><?php printf( esc_attr__( 'Results for : "%s"', 'amio' ), '<em>' . get_search_query() . '</em>' ); ?></h3>
							
						</div>
						<!-- End Heading -->
						
						
						
					</div>
					<!-- End Section Content -->
					
				</div>
				<!-- End Header -->
 <!-- About -->
<div id="blog">
<!-- Section Content -->
	<div class="section-content">
	<?php defined('ABSPATH') or die("No script kiddies please!");?>
<?php $amio_options = get_option('amio'); ?> 
<div class="row">
	<div class="col-md-8">
								<?php if(have_posts()) : ?>
								<?php global $post, $post_id;?>
								<?php while ( have_posts() ) : the_post();?>
							<!-- Blog Post -->
								<div class="blog-post">
								<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
								
									<?php if( has_post_format( 'video' ) !='') :?>
									<?php get_template_part('template-parts/blog/post-type/video');?>
									<?php elseif( has_post_format( 'gallery' ) !='') :?>
									<?php get_template_part('template-parts/blog/post-type/gallery');?>
									<?php elseif( has_post_format( 'image' ) !='') :?>
									<?php get_template_part('template-parts/blog/post-type/image');?>
									<?php elseif( has_post_format( 'audio' ) !='') :?>
									<?php get_template_part('template-parts/blog/post-type/audio');?>
									<?php else:?>
									<?php endif;?>
									
									<!-- Post Title -->
									<h3 class="t-left f-medium dark ultrabold uppercase"><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
									
									<!-- Post Information -->
									<ul class="t-left lato f-small">
										<li class="no-margin no-border no-padding"><?php esc_attr_e('Author: ','amio');?><a href="<?php the_permalink();?>"><?php the_author();?></a></li>
										<li><?php esc_attr_e('Date: ','amio');?><a href="<?php the_permalink();?>"><?php the_time( get_option( 'date_format' ) ); ?></a></li>
										<li><?php esc_attr_e('In: ','amio');?><?php the_category(', ');?></li>
										<?php
										if( has_tag() ) {?>
										<li><?php the_tags();?></li>
										<?php }?>
									</ul>
									<!-- End Post Information -->
									
									
									<!-- Post Snippet -->
									<p class="t-left">
										<?php
										$amio_excerpt= substr(strip_tags($post->post_content), 0, 183);
										update_post_meta(get_the_ID(), 'amio_excerpt', $amio_excerpt);
										echo esc_html($amio_excerpt);
										?>
									</p>
									<!-- End Post Snippet -->
									
									<!-- Button -->
									<a href="<?php the_permalink();?>" class="button float-l"><?php esc_attr_e('Read More','amio');?></a>
									
								</div>
								</div>
								<!-- End Blog Post -->
								<?php endwhile; ?>
							    <?php wp_reset_postdata();?>
								<?php if (function_exists("amio_pagination")) { ?>
								<?php amio_pagination($wp_query->max_num_pages);
								?>
								<?php } ?>
								<?php else : ?>
								<h3 class="t-left f-medium dark ultrabold uppercase"><?php esc_attr_e('No Post Found','amio');?></h3>
								<?php endif;?>
	</div>
	<div class="col-md-4">
		<?php
		if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Blog Sidebar')): 
		endif;
		?>
	</div>
</div>
	</div>		
</div>		
<?php get_footer(); ?>	