<?php
$amio_options = get_option('amio');
// Enqueue Style
require get_template_directory() . '/includes/style.php';
// Enqueue JS
require get_template_directory() . '/includes/js.php';
// redux options	
require get_template_directory() . '/includes/amio-options.php';
require get_template_directory() . '/includes/AfterSetupTheme.php';
require get_template_directory() . '/includes/functions.php';
require get_template_directory() . '/pagination.php';
if ( ! isset( $content_width ) ) $content_width = 900;	
// register nav menu
function amio_register_menus() {
register_nav_menus( array( 
'top-menu' => esc_attr__( 'Primary menu','amio' ),
    )
	 );
		}
add_action('init', 'amio_register_menus');

add_action( 'after_setup_theme', 'amio_setup' );
function amio_setup() {
// Theme Support  
	function amio_editor_styles() {
    add_editor_style( 'style.css' );
}
	add_action( 'after_setup_theme', 'amio_add_editor_styles' );
	add_action( 'after_setup_theme', 'amio_lang_setup' );
	function amio_lang_setup(){
    load_theme_textdomain('amio', get_template_directory() . '/languages');
    }
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'custom-background' );
	add_theme_support( 'custom-header' );
	add_theme_support( "title-tag" );
	add_theme_support( 'post-formats', array('image','video', 'gallery', 'audio') );
	add_post_type_support( 'portgallery', 'post-formats' );

}
// Word Limit 
	function amio_string_limit_words($string, $word_limit)
	{
	$words = explode(' ', $string, ($word_limit + 1));
	if(count($words) > $word_limit)
	array_pop($words);
	return implode(' ', $words);
	}
// Add post thumbnail functionality
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 559, 220, true ); // Normal post thumbnails
	add_image_size( 'amio-button', 96, 50, true ); // post details
	add_image_size( 'amio-slider', 480, 600, true ); // post details
	
// How comments are displayed


function amio_comment($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment;
    extract($args, EXTR_SKIP);

    if ( 'div' == $args['style'] ) {
      $tag = 'div';
      $add_below = 'comment';
    } else {
      $tag = 'li';
      $add_below = 'div-comment';
    }
?>

    <<?php echo esc_attr($tag); ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> data-uk-scrollspy="{cls:'uk-animation-fade'}">
    <?php if ( 'div' != $args['style'] ) : ?>
	<div id="div-comment-<?php comment_ID() ?>" class="posted-comment comment clearfix">
	
    <?php endif; ?>
    <div class="row">
	<div class="col-md-12">
	<!-- User Image -->
	<a href="#" class="float-l"><?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['avatar_size'] ); ?></a>
	</div>
	<!-- Column -->
	<div class="col-md-12">
	<!-- Date -->
	<h3 class="f-normal ultrabold uppercase t-left float-l"><?php printf(__('%s','amio'), get_comment_author_link()) ?> <?php esc_attr_e('Says: ','amio');?><span class="gray1 normal lato"><?php esc_attr_e('on ','amio');?><?php the_time( get_option( 'date_format' ) ); ?></span></h3>
	</div>
	<!-- End Column -->
	</div>
	<!-- Row -->
	<div class="row">
	<!-- Column -->
	<div class="col-md-12 t-left no-margin">
	<!-- Comment -->
	<?php comment_text() ?>
	<!-- Reply -->
	<?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
	<br></br>
	<?php if ($comment->comment_approved == '0') : ?>
    <em class="comment-awaiting-moderation"><?php esc_attr_e('Your comment is awaiting moderation.','amio') ?></em>
    <br />
   <?php endif; ?>
	</div>
	<!-- End Column -->
	</div>
	<!-- End Row -->
    
	 
          
     
 
    
    <?php if ( 'div' != $args['style'] ) : ?>
    
    </div>
    <?php endif; ?>
<?php
        }
// create sidebar & widget area

if(function_exists('register_sidebar')) {

function amio_theme_slug_widgets_init() {
    register_sidebar( array(
        'name' => esc_attr__( 'Blog Sidebar', 'amio' ),
        'id' => 'sidebar-1',
        'description' => esc_attr__( 'This area for Blog widgets.', 'amio' ),
        'before_widget' => '<div id="%1$s" class="widget sidebar t-left %2$s">',
		'after_widget'  => '</div>', 
		'before_title'  => '<h3 class="f-normal ultrabold">', 
		'after_title'   => '</h3> <div class="separator-small"></div>'
    ) );
}
add_action( 'widgets_init', 'amio_theme_slug_widgets_init' );

if (class_exists('WooCommerce')) {
function amio_theme_slug_widgets___init() {
    register_sidebar( array(
        'name' => esc_attr__( 'WOOCOMMERCE Sidebar','amio' ),
        'id' => 'sidebar-2',
        'description' => esc_attr__( 'This area for All WOOCOMMERCE Widget.','amio' ),
        'before_widget' => '<div id="%1$s" class="widget sidebar t-left %2$s">',
		'after_widget'  => '</div>', 
		'before_title'  => '<h3 class="f-normal ultrabold uppercase">', 
		'after_title'   => '</h3> <div class="separator-small"></div>'
    ) );
}
add_action( 'widgets_init', 'amio_theme_slug_widgets___init' );
}
}

if(function_exists('vc_set_as_theme')) vc_set_as_theme();
// Initialising Shortcodes
if (class_exists('WPBakeryVisualComposerAbstract')) {
	function requireVcExtend(){
		require_once get_template_directory() . '/extendvc/extend-vc.php';
	}
	add_action('init', 'requireVcExtend',2);
}

/* CHECK WOOCOMMERCE IS ACTIVE
  ================================================== */ 
  if ( ! function_exists( 'amio_woocommerce_activated' ) ) {
    function amio_woocommerce_activated() {
      if ( class_exists( 'woocommerce' ) ) {
        return true;
      } else {
        return false;
      }
    }
  }
add_theme_support( 'woocommerce' );

remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
remove_action('woocommerce_pagination', 'woocommerce_pagination', 10);
function woocommerce_pagination() {
		amio_pagination(); 		
	}
add_action( 'woocommerce_pagination', 'woocommerce_pagination', 10);


/**
     * Get current users preference
     * @return int
     */
    function amio_get_products_per_page(){
 
        global $woocommerce;
 
        $amio_default = 9;
        $amio_count = $amio_default;
        $amio_options = amio_get_products_per_page_options();
 
        // capture form data and store in session
        if(isset($_POST['jc-woocommerce-products-per-page'])){
 
            // set products per page from dropdown
            $amio_products_max = intval($_POST['jc-woocommerce-products-per-page']);
            if($amio_products_max != 0 && $amio_products_max >= -1){
 
            	if(is_user_logged_in()){
 
            		$amio_user_id = get_current_user_id();
    		    	$amio_limit = get_user_meta( $amio_user_id, '_product_per_page', true );
 
    		    	if(!$amio_limit){
    		    		add_user_meta( $amio_user_id, '_product_per_page', $amio_products_max);
    		    	}else{
    		    		update_user_meta( $amio_user_id, '_product_per_page', $amio_products_max, $amio_limit);
    		    	}
            	}
 
                $woocommerce->session->amio_product_per_page = $amio_products_max;
                return $amio_products_max;
            }
        }
 
        // load product limit from user meta
        if(is_user_logged_in() && !isset($woocommerce->session->amio_product_per_page)){
 
            $amio_user_id = get_current_user_id();
            $amio_limit = get_user_meta( $amio_user_id, '_product_per_page', true );
 
            if(array_key_exists($amio_limit, $amio_options)){
                $woocommerce->session->amio_product_per_page = $amio_limit;
                return $amio_limit;
            }
        }
 
        // load product limit from session
        if(isset($woocommerce->session->amio_product_per_page)){
 
            // set products per page from woo session
            $amio_products_max = intval($woocommerce->session->amio_product_per_page);
            if($amio_products_max != 0 && $amio_products_max >= -1){
                return $amio_products_max;
            }
        }
 
        return $amio_count;
    }
    add_filter('loop_shop_per_page','amio_get_products_per_page');
 
    /**
     * Fetch list of avaliable options
     * @return array
     */
    function amio_get_products_per_page_options(){
    	$amio_options = apply_filters( 'amio_products_per_page', array(
    		9 => esc_attr__('9', 'amio'),
    		18 => esc_attr__('18', 'amio'),
    		36 => esc_attr__('36', 'amio'),
    		72 => esc_attr__('72', 'amio'),
    		144 => esc_attr__('144', 'amio'),
        ));
 
    	return $amio_options;
    }
 
    /**
     * Display dropdown form to change amount of products displayed
     * @return void
     */
    function amio_woocommerce_products_per_page(){
 
        $amio_options = amio_get_products_per_page_options();
 
        $amio_current_value = amio_get_products_per_page();
        ?>
       
            <form action="" method="POST" class="woocommerce-products-per-page">
                <select name="jc-woocommerce-products-per-page" onchange="this.form.submit()">
                <?php foreach($amio_options as $amio_value => $amio_name): ?>
                    <option value="<?php echo esc_attr($amio_value); ?>" <?php selected($amio_value, $amio_current_value); ?>><?php esc_attr_e('Show on page: ','amio');?><?php echo esc_attr($amio_name); ?></option>
                <?php endforeach; ?>
                </select>
            </form>
        
        <?php
    }
 
add_action('woocommerce_before_shop_loop', 'amio_woocommerce_products_per_page', 25);
add_filter( 'woocommerce_output_related_products_args', 'amio_related_products_args' );
  function amio_related_products_args( $args ) {

	$args['posts_per_page'] = 3; // 3 related products
	$args['columns'] = 3; // arranged in 3 columns
	return $args;
}

/* Include Meta Box Framework */
define( 'RWMB_URL', trailingslashit( get_template_directory_uri() . '/includes/metaboxes' ) );
define( 'RWMB_DIR', trailingslashit( get_template_directory() . '/includes/metaboxes' ) );

require_once RWMB_DIR . 'meta-box.php';
