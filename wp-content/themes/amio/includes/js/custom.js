/*

*	Amio - Minimal Multipurpose Portfolio Template
* 	Author: OrbmobThemes
* 	Copyright (C) 2016 Orbmob
* 	This is a premium product available exclusively here : https://themeforest.net/user/orbmobthemes/portfolio


*/







/*====================================================


	Table of Contents
	
	
		01. Loading Screen 
		
		02. Sliders 
		
			+Image Slider 
		
		04. Lightbox

	
	
====================================================*/



jQuery(document).ready(function($) {
	"use strict";

	
jQuery('#s').attr('placeholder','Type and hit enter...');	
/*======================

	01. Loading Screen 

========================*/



	setTimeout(function(){
		jQuery('body').addClass('loaded');
		jQuery('h1').css('color','#222222');
	}, 0);

	
	
	
	
	

	
/*======================

	04. Lightbox 

========================*/





	lightbox.option({
      'resizeDuration': 200,
      'wrapAround': true
    });
	
	jQuery(".rotate").textrotator({
						  animation: "flipUp", // You can pick the way it animates when rotating through words. Options are dissolve (default), fade, flip, flipUp, flipCube, flipCubeUp and spin.
						  separator: ",", // If you don't want commas to be the separator, you can define a new separator (|, &, * etc.) by yourself using this field.
						  speed: 3000 // How many milliseconds until the next word show.
	});
	
	jQuery(".rotate-filf").textrotator({
						  animation: "flip", // You can pick the way it animates when rotating through words. Options are dissolve (default), fade, flip, flipUp, flipCube, flipCubeUp and spin.
						  separator: ",", // If you don't want commas to be the separator, you can define a new separator (|, &, * etc.) by yourself using this field.
						  speed: 3000 // How many milliseconds until the next word show.
					});
	
jQuery("ul.wr-mega-menu").each(function(i){
jQuery(this).addClass('nav navbar-nav f-small normal raleway');
});	

jQuery( ".amio-navbar-toggle" ).click(function() {
  jQuery( ".amio-navbar-collapse" ).slideToggle( "slow", function() {
  });
});

jQuery(".amio-collapse style").replaceWith(function() { return "<div class='wr-hidden'>" + this.innerHTML + "</div>"; });
	
});