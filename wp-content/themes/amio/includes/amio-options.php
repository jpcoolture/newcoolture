<?php
    /**
     * ReduxFramework Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }


    // This is your option name where all the Redux data is stored.
    $opt_name = "amio";

    // This line is only for altering the demo. Can be easily removed.
    $opt_name = apply_filters( 'amio/opt_name', $opt_name );

    

    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
		'class'                => 'admin-color-amio',
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'menu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title'           => __( 'Amio Options', 'amio' ),
        'page_title'           => __( 'Amio Options', 'amio' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => '',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => true,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => true,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-portfolio',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 50,
        // Choose an priority for the admin bar menu
        'global_variable'      => '',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => false,
        // Show the time the page took to load, etc
        'update_notice'        => false,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => true,
        // Enable basic customizer support
        //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
        //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

        // OPTIONAL -> Give you extra features
        'page_priority'        => null,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => '',
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => '',
        // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
        // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
        'use_cdn'              => true,
        // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'red',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'amio'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'amio'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

    
    // Panel Intro text -> before the form
    if ( ! isset( $args['global_variable'] ) || $args['global_variable'] !== false ) {
        if ( ! empty( $args['global_variable'] ) ) {
            $v = $args['global_variable'];
        } else {
            $v = str_replace( '-', '_', $args['opt_name'] );
        }
        $args['intro_text'] = sprintf( __( '', 'amio' ), $v );
    } else {
        $args['intro_text'] = __( '', 'amio' );
    }

    // Add content after the form.
    $args['footer_text'] = __( '', 'amio' );

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */


    /*
     * ---> START HELP TABS
     */

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => esc_attr__( 'Theme Information 1', 'amio' ),
            'content' => esc_attr__( 'This is the tab content, HTML is allowed.', 'amio' )
        ),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => esc_attr__( 'Theme Information 2', 'amio' ),
            'content' => esc_attr__( 'This is the tab content, HTML is allowed.', 'amio' )
        )
    );
    Redux::setHelpTab( $opt_name, $tabs );

    // Set the help sidebar
    $content = esc_attr__( 'This is the sidebar content, HTML is allowed.', 'amio' );
    Redux::setHelpSidebar( $opt_name, $content );


    /*
     * <--- END HELP TABS
     */


    /*
     *
     * ---> START SECTIONS
     *
     */

    /*

        As of Redux 3.5+, there is an extensive API. This API can be used in a mix/match mode allowing for


     */

    // ACTUAL DECLARATION OF SECTIONS
                Redux::setSection( $opt_name, array(
                    'title'  => esc_attr__( 'General Settings', 'amio' ),
                    'desc'   => esc_attr__( '', 'amio' ),
                    'icon'   => 'el-icon-home-alt',
                    // 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
                    'fields' => array(
					 
					  array(
							'id' => 'wr-site-logo',
							'type' => 'info',
		                    'notice' => true,
		                    'style' => 'info',
							'title' => esc_attr__('Site Logo', 'amio'),
							'desc' => esc_attr__('', 'amio')
						
					),
					 
					 array(
							'id' => 'wr-logo-main',
							'type' => 'button_set',
							'title' => esc_attr__('Select Logo Type', 'amio'),
							'subtitle' => esc_attr__('', 'amio'),
							'compiler' => 'true',
							'options' => array(
									'st1'=> esc_attr__('Text Logo', 'amio'),
									'st2' => esc_attr__('Image Logo', 'amio')
					
							),
							'default'  => 'st1'
					),
                    array(
							'id' => 'logopic',
							'type' => 'media',
							'compiler' => 'true',
							'title' => esc_attr__('Upload Logo', 'amio'),
							'subtitle' => esc_attr__('', 'amio'),
							'required' => array('wr-logo-main', '=' , 'st2')
					),
					
					$fields = array(
					'id'       => 'opt_headerlogo_dimensions',
					'type'     => 'dimensions',
					'units'    => array('em','px','%'),
					'output' => array('.navbar-brand img'),
					'title'    => __('Logo Size', 'amio'),
					'subtitle' => __('', 'amio'),
					'desc'     => __('Optional', 'amio'),
					'default'  => array(
						'Width'   => '', 
						'Height'  => ''
					),
					'required' => array('wr-logo-main', '=' , 'st2')
				),
					
					array(
							'id' => 'wrlogotxt',
							'type' => 'text',
							'title' => esc_attr__('Logo Text', 'amio'),
							'subtitle' => esc_attr__('', 'amio'),
							'required' => array('wr-logo-main', '=' , 'st1')
					
					),
					
					
					array(
							'id' => 'wr-back-loading',
							'type' => 'info',
		                    'notice' => true,
		                    'style' => 'info',
							'title' => esc_attr__('Site Loading Effect.', 'amio'),
							'desc' => esc_attr__('', 'amio')
							
					  ),
					  
					 
					array(
							'id' => 'wr-loading-page',
							'type' => 'button_set',
							'title' => esc_attr__('Site Loader', 'amio'),
							'subtitle' => esc_attr__('', 'amio'),
							'compiler' => 'true',
							'options' => array(
									'st1'=> esc_attr__('Enable', 'amio'),
									'st2' => esc_attr__('Diable', 'amio')
					
							),
							'default'  => 'st2'
					),
					
					array(
							'id' => 'loaderlogopic',
							'type' => 'media',
							'compiler' => 'true',
							'title' => esc_attr__('Upload Logo', 'amio'),
							'subtitle' => esc_attr__('', 'amio'),
							'required' => array('wr-loading-page', '=' , 'st1')
					),
					
					$fields = array(
					'id'       => 'opt_loadinglogo_dimensions',
					'type'     => 'dimensions',
					'units'    => array('em','px','%'),
					'output' => array('.loader-img img'),
					'title'    => __('Logo Size', 'amio'),
					'subtitle' => __('', 'amio'),
					'desc'     => __('Optional', 'amio'),
					'default'  => array(
						'Width'   => '', 
						'Height'  => ''
					),
					'required' => array('wr-loading-page', '=' , 'st1')
				),
					
					
									
					
					
				  )
               ) );

			
		Redux::setSection( $opt_name, array(
                    'icon'   => 'el-icon-bullhorn',
                    'title'  => esc_attr__( 'Blog & Portfolio Options', 'amio' ),
                    'fields' => array(
					
					array(
							'id' => 'wr-blog-opt',
							'type' => 'info',
		                    'notice' => true,
		                    'style' => 'info',
							'title' => esc_attr__('Blog Options', 'amio'),
							'desc' => esc_attr__(' ', 'amio')
							
					  ),

					array(
							'id' => 'blog-sidebar',
							'type' => 'button_set',
							'title' => esc_attr__('Standard Blog Sidebar', 'amio'),
							'subtitle' => esc_attr__('Select Blog Page Sidebar Position.', 'amio'),
							'desc' => '',
							'options' => array(
									'st1'=> esc_attr__('Left Sidebar', 'amio'),
									'st2' => esc_attr__('Right Sidebar', 'amio'),
									'st3' => esc_attr__('Full Width', 'amio'),
									'st4' => esc_attr__('Masonary', 'amio'),
									
							),
							'default'  => 'st2'
					),
					
					array(
							'id' => 'blogtitle',
							'type' => 'text',
							'title' => esc_attr__('Index Title ', 'amio'),
							'subtitle' => esc_attr__('', 'amio'),
						
					),
					
					array(
							'id' => 'blogsubtitle',
							'type' => 'text',
							'title' => esc_attr__('Index Sub Title ', 'amio'),
							'subtitle' => esc_attr__('', 'amio'),
						
					),
					
					array(
							'id' => 'wr-blog-de-opt',
							'type' => 'info',
		                    'notice' => true,
		                    'style' => 'info',
							'title' => esc_attr__('Blog Details Page Style.', 'amio'),
							'desc' => esc_attr__(' ', 'amio')
							
					 ),
					 
					 array(
							'id' => 'blogdetst',
							'type' => 'button_set',
							'title' => esc_attr__('Select Blog Details Page Style', 'amio'),
							'subtitle' => esc_attr__('', 'amio'),
							'desc' => '',
							'options' => array(
									'st1'=> esc_attr__('Left Sidebar', 'amio'),
									'st2' => esc_attr__('Right Sidebar', 'amio'),
									'st3' => esc_attr__('Full Width', 'amio'),
									
							),
							'default'  => 'st2'
					),
					
					array(
							'id' => 'wr-index-opt',
							'type' => 'info',
		                    'notice' => true,
		                    'style' => 'info',
							'title' => esc_attr__('Background Option For Blog Detilas Page.', 'amio'),
							'desc' => esc_attr__(' ', 'amio')
							
					 ),
					
					array(
							'id' => 'header-wr-index-img',
							'type' => 'media',
							'compiler' => 'true',
							'title' => esc_attr__('Upload Image', 'amio'),
							'subtitle' => esc_attr__('', 'amio'),
							
						
					),
					
					
					array(
							'id' => 'wr-404-opt',
							'type' => 'info',
		                    'notice' => true,
		                    'style' => 'info',
							'title' => esc_attr__('Background Option For 404 Page.', 'amio'),
							'desc' => esc_attr__(' ', 'amio')
							
					  ),
					
					array(
							'id' => 'header-wr-404-img',
							'type' => 'media',
							'compiler' => 'true',
							'title' => esc_attr__('Upload Image', 'amio'),
							'subtitle' => esc_attr__('', 'amio'),
							
						
					),
					
					
					
                    )
                ) );
				
				Redux::setSection( $opt_name, array(
                    'icon'   => 'el-icon-briefcase',
                    'title'  => esc_attr__( 'Translation Options', 'amio' ),
                    'fields' => array(
					
					
					array(
							'id' => 'wr-rdmore',
							'type' => 'text',
							'title' => esc_attr__('Read More', 'amio'),
							'subtitle' => esc_attr__('', 'amio'),
						
					),
					
					array(
							'id' => 'wr-author',
							'type' => 'text',
							'title' => esc_attr__('Author:', 'amio'),
							'subtitle' => esc_attr__('', 'amio'),
						
					),
					
					array(
							'id' => 'wr-date',
							'type' => 'text',
							'title' => esc_attr__('Date:', 'amio'),
							'subtitle' => esc_attr__('', 'amio'),
						
					),
					
					array(
							'id' => 'wr-in',
							'type' => 'text',
							'title' => esc_attr__('In:', 'amio'),
							'subtitle' => esc_attr__('', 'amio'),
						
					),
					
					array(
							'id' => 'wr-say',
							'type' => 'text',
							'title' => esc_attr__('Say something!', 'amio'),
							'subtitle' => esc_attr__('', 'amio'),
						
					),
					
					array(
							'id' => 'wr-name',
							'type' => 'text',
							'title' => esc_attr__('Your Name', 'amio'),
							'subtitle' => esc_attr__('', 'amio'),
						
					),
					
					array(
							'id' => 'wr-email',
							'type' => 'text',
							'title' => esc_attr__('Your Email', 'amio'),
							'subtitle' => esc_attr__('', 'amio'),
						
					),
					
					array(
							'id' => 'wr-comment',
							'type' => 'text',
							'title' => esc_attr__('Your Comment', 'amio'),
							'subtitle' => esc_attr__('', 'amio'),
						
					),
					
					array(
							'id' => 'wr-post',
							'type' => 'text',
							'title' => esc_attr__('Post', 'amio'),
							'subtitle' => esc_attr__('', 'amio'),
						
					),
					
					
					
					
                    )
                ) );
				
				if (class_exists('WooCommerce')) {
				Redux::setSection( $opt_name, array(
                    'icon'   => 'el-icon-bullhorn',
                    'title'  => esc_attr__( 'Shop Options', 'amio' ),
                    'fields' => array(
					
					array(
							'id' => 'wr-shop-opt',
							'type' => 'info',
		                    'notice' => true,
		                    'style' => 'info',
							'title' => esc_attr__('Shop Header Options', 'amio'),
							'desc' => esc_attr__(' ', 'amio')
							
					  ),

					
					
					array(
							'id' => 'shopsubtitle',
							'type' => 'text',
							'title' => esc_attr__('Sub Title ', 'amio'),
							'subtitle' => esc_attr__('Shop page sub title', 'amio'),
							
							
							
					),
					
					
					
					
                    )
                ) );
				}
				
		Redux::setSection( $opt_name, array(
                    'icon'   => 'el-icon-map-marker',
                    'title'  => esc_attr__( 'Google Map Options', 'amio' ),
                    'fields' => array(
					
					
					

					
					array(
							'id' => 'wr-map-opt',
							'type' => 'info',
		                    'notice' => true,
		                    'style' => 'info',
							'title' => esc_attr__('Map Options', 'amio'),
							'desc' => esc_attr__(' ', 'amio')
							
					  ),

					array(
							'id' => 'maploc',
							'type' => 'text',
							'title' => esc_attr__('Map Location ', 'amio'),
							'subtitle' => esc_attr__('', 'amio'),
							'desc' => esc_attr__('e.x: 40.7060895 ', 'amio')
							
							
					),
					
					array(
							'id' => 'maploc2',
							'type' => 'text',
							'title' => esc_attr__('Map Location ', 'amio'),
							'subtitle' => esc_attr__('', 'amio'),
							'desc' => esc_attr__('e.x: -73.999053 ', 'amio')
							
							
					),
					
					array(
							'id' => 'userdata',
							'type' => 'text',
							'title' => esc_attr__('User Data ', 'amio'),
							'subtitle' => esc_attr__('', 'amio'),
							
							
					),
					
					array(
							'id' => 'mapmarker',
							'type' => 'media',
							'compiler' => 'true',
							'title' => esc_attr__('Map Marker', 'amio'),
							'subtitle' => esc_attr__('', 'amio')
					),
					
					
					
					
					
					
					
                    )
                ) );
				
		Redux::setSection( $opt_name, array(
                    'icon'   => 'el-icon-leaf',
                    'title'  => esc_attr__( 'Social Options', 'amio' ),
                    'fields' => array(
					
					
					
					array(
							'id' => 'wr-social-target',
							'type' => 'button_set',
							'title' => esc_attr__('Link Target', 'amio'),
							'subtitle' => esc_attr__('', 'amio'),
							'desc' => '',
							'options' => array(
									'self'=> esc_attr__('Self', 'amio'),
									'blank' => esc_attr__('Blank', 'amio'),
									
							),
							'default'  => 'self'
					),
					
										
					array(
							'id' => 'behance',
							'type' => 'text',
							'title' => esc_attr__('Behance URL ', 'amio'),
							'subtitle' => esc_attr__('Write Social URL', 'amio'),
							
					
					),
					
					array(
							'id' => 'pinterest',
							'type' => 'text',
							'title' => esc_attr__('Pinterest URL ', 'amio'),
							'subtitle' => esc_attr__('Write Social URL', 'amio'),
							
					),
					
					array(
							'id' => 'twitter',
							'type' => 'text',
							'title' => esc_attr__('Twitter URL ', 'amio'),
							'subtitle' => esc_attr__('Write Social URL', 'amio'),
								
					),
					
					array(
							'id' => 'gplus',
							'type' => 'text',
							'title' => esc_attr__('Google URL ', 'amio'),
							'subtitle' => esc_attr__('Write Social URL', 'amio'),
							
					),
					
					array(
							'id' => 'facebook',
							'type' => 'text',
							'title' => esc_attr__('Facebook URL ', 'amio'),
							'subtitle' => esc_attr__('Write Social URL', 'amio'),
							
					),
					
					array(
							'id' => 'linkedin',
							'type' => 'text',
							'title' => esc_attr__('Linkedin URL ', 'amio'),
							'subtitle' => esc_attr__('Write Social URL', 'amio'),
							
					),
					
					array(
							'id' => 'instagram',
							'type' => 'text',
							'title' => esc_attr__('Instagram URL ', 'amio'),
							'subtitle' => esc_attr__('Write Social URL', 'amio'),
							
							
					),
					
					array(
							'id' => 'youtube',
							'type' => 'text',
							'title' => esc_attr__('Youtube URL ', 'amio'),
							'subtitle' => esc_attr__('Write Social URL', 'amio'),
						
					),
					
					array(
							'id' => 'vimeo',
							'type' => 'text',
							'title' => esc_attr__('Vimeo URL ', 'amio'),
							'subtitle' => esc_attr__('Write Social URL', 'amio'),
						
					),
					
					array(
							'id' => 'git',
							'type' => 'text',
							'title' => esc_attr__('Git URL ', 'amio'),
							'subtitle' => esc_attr__('Write Social URL', 'amio'),
					
					),
					
					array(
							'id' => 'slack',
							'type' => 'text',
							'title' => esc_attr__('Slack URL ', 'amio'),
							'subtitle' => esc_attr__('Write Social URL', 'amio'),
					
					),
					
					array(
							'id' => 'xing',
							'type' => 'text',
							'title' => esc_attr__('Xing URL ', 'amio'),
							'subtitle' => esc_attr__('Write Social URL', 'amio'),
					
					),
					
					array(
							'id' => 'dribbble',
							'type' => 'text',
							'title' => esc_attr__('Dribbble URL ', 'amio'),
							'subtitle' => esc_attr__('Write Social URL', 'amio'),
							
							
					),
					
					array(
							'id' => 'soundcloud',
							'type' => 'text',
							'title' => esc_attr__('Soundcloud URL ', 'amio'),
							'subtitle' => esc_attr__('Write Social URL', 'amio'),
							
							
					),
					
					array(
							'id' => 'tumblr',
							'type' => 'text',
							'title' => esc_attr__('Tumblr URL ', 'amio'),
							'subtitle' => esc_attr__('Write Social URL', 'amio'),
							
							
					),
					
					
                    )
                ) );
				
				
				Redux::setSection( $opt_name, array(
                    'icon'   => 'el-icon-text-width',
                    'title'  => esc_attr__( 'Typography', 'amio' ),
                    'fields' => array(     

						array(
			                'id' => 'notice_critical11',
			                'type' => 'info',
			                'notice' => true,
			                'style' => 'success',
			                'title' => esc_attr__('Entry Headings', 'amio'),
			                'desc' => esc_attr__('Entry Headings in posts/pages', 'amio')
			            ),					
                        array(
                            'id'          => 'typography-h1',
                            'type'        => 'typography', 
                            'title'       => esc_attr__('H1', 'amio'),
                            'google'      => true, 
                            'font-backup' => false,
                            'output'      => array('h1'),
                            'units'       =>'px',
                            'subtitle'    => esc_attr__('Specify the Heading font properties.', 'amio'),
                            'default'     => array(
                            'color'       => false,
                            'font-style'  => false,
                            'font-family' => false,
                            'google'      => true,
                            'font-size'   => false,
                            'line-height' => false,
                            ),
						),	
                        array(
                            'id'          => 'typography-h2',
                            'type'        => 'typography', 
                            'title'       => esc_attr__('H2', 'amio'),
                            'google'      => true, 
                            'font-backup' => false,
                            'output'      => array('h2, .service h2'),
                            'units'       =>'px',
                            'subtitle'    => esc_attr__('Specify the Heading font properties.', 'amio'),
                            'default'     => array(
                            'color'       => false,
                            'font-style'  => false,
                            'font-family' => false,
                            'google'      => true,
                            'font-size'   => false,
                            'line-height' => false,
                            ),      
                        ),
                        array(
                            'id'          => 'typography-h3',
                            'type'        => 'typography', 
                            'title'       => esc_attr__('H3', 'amio'),
                            'google'      => true, 
                            'font-backup' => false,
                            'output'      => array('h3'),
                            'units'       =>'px',
                            'subtitle'    => esc_attr__('Specify the Heading font properties.', 'amio'),
                            'default'     => array(
                            'color'       => false,
                            'font-style'  => false,
                            'font-family' => false,
                            'google'      => true,
                            'font-size'   => false,
                            'line-height' => false,
                            ),
						),	
                        array(
                            'id'          => 'typography-h4',
                            'type'        => 'typography', 
                            'title'       => esc_attr__('H4', 'amio'),
                            'google'      => true, 
                            'font-backup' => false,
                            'output'      => array('h4'),
                            'units'       =>'px',
                            'subtitle'    => esc_attr__('Specify the Heading font properties.', 'amio'),
                            'default'     => array(
                            'color'       => false,
                            'font-style'  => false,
                            'font-family' => false,
                            'google'      => true,
                            'font-size'   => false,
                            'line-height' => false,
                            ),	
						),	
                        array(
                            'id'          => 'typography-h5',
                            'type'        => 'typography', 
                            'title'       => esc_attr__('H5', 'amio'),
                            'google'      => true, 
                            'font-backup' => false,
                            'output'      => array('h5'),
                            'units'       =>'px',
                            'subtitle'    => esc_attr__('Specify the Heading font properties.', 'amio'),
                            'default'     => array(
                            'color'       => false,
                            'font-style'  => false,
                            'font-family' => false,
                            'google'      => true,
                            'font-size'   => false,
                            'line-height' => false,
                            ),
						),	
                        array(
                            'id'          => 'typography-h6',
                            'type'        => 'typography', 
                            'title'       => esc_attr__('H6', 'amio'),
                            'google'      => true, 
                            'font-backup' => false,
                            'output'      => array('h6'),
                            'units'       =>'px',
                            'subtitle'    => esc_attr__('Specify the Heading font properties.', 'amio'),
                            'default'     => array(
                            'color'       => false,
                            'font-style'  => false,
                            'font-family' => false,
                            'google'      => true,
                            'font-size'   => false,
                            'line-height' => false,
                            ),
						),

						array(
                            'id'          => 'typography-p',
                            'type'        => 'typography', 
                            'title'       => esc_attr__('p', 'amio'),
                            'google'      => true, 
                            'font-backup' => false,
                            'output'      => array('p, .service p, p.dark'),
                            'units'       =>'px',
                            'subtitle'    => esc_attr__('Specify the content font properties.', 'amio'),
                            'default'     => array(
                            'color'       => false,
                            'font-style'  => false,
                            'font-family' => false,
                            'google'      => true,
                            'font-size'   => false,
                            'line-height' => false,
                            ),
						),
						
						array(
                            'id'          => 'typography-a',
                            'type'        => 'typography', 
                            'title'       => esc_attr__('a', 'amio'),
                            'google'      => true, 
                            'font-backup' => false,
                            'output'      => array('a, a h3, a h2, a p, a h4, a h5, a h6'),
                            'units'       =>'px',
                            'subtitle'    => esc_attr__('Specify the link font properties.', 'amio'),
                            'default'     => array(
                            'color'       => false,
                            'font-style'  => false,
                            'font-family' => false,
                            'google'      => true,
                            'font-size'   => false,
                            'line-height' => false,
                            ),
						),

						array(
			                'id' => 'notice_menucritical11',
			                'type' => 'info',
			                'notice' => true,
			                'style' => 'success',
			                'title' => esc_attr__('Menu Font Options', 'amio'),
			                'desc' => esc_attr__('Menu Item font properties', 'amio')
			            ),					
                        array(
                            'id'          => 'typography-menu-main',
                            'type'        => 'typography', 
                            'title'       => esc_attr__('Menu Item', 'amio'),
                            'google'      => true, 
                            'font-backup' => false,
                            'output'      => array('.nav li a'),
                            'units'       =>'px',
                            'subtitle'    => esc_attr__('Specify the menu item font properties.', 'amio'),
                            'default'     => array(
                            'color'       => false,
                            'font-style'  => false,
                            'font-family' => false,
                            'google'      => true,
                            'font-size'   => false,
                            'line-height' => false,
                            ),
						),

						array(
			                'id' => 'notice_menucritical1134r',
			                'type' => 'info',
			                'notice' => true,
			                'style' => 'success',
			                'title' => esc_attr__('Menu Font Hover & Active Options', 'amio'),
			                'desc' => esc_attr__('Menu Item font properties', 'amio')
			            ),
						
						array(
                            'id'          => 'typography-menu-mainac',
                            'type'        => 'typography', 
                            'title'       => esc_attr__('Menu Item Active & Hover', 'amio'),
                            'google'      => true, 
                            'font-backup' => false,
                            'output'      => array('#amio-mega .wr-megamenu-container ul.wr-mega-menu li.wr-megamenu-item:hover > a.menu-item-link, #amio-mega .wr-megamenu-container .current_page_item > a, #amio-mega .wr-megamenu-container .current_page_ancestor > a, #amio-mega .wr-megamenu-container .current-menu-item > a, #amio-mega .wr-megamenu-container .current-menu-ancestor > a, .nav > li > a:focus, .nav > li > a:hover, .nav > li.current-menu-item > a'),
                            'units'       =>'px',
                            'subtitle'    => esc_attr__('Specify the menu item font properties.', 'amio'),
                            'default'     => array(
                            'color'       => false,
                            'font-style'  => false,
                            'font-family' => false,
                            'google'      => true,
                            'font-size'   => false,
                            'line-height' => false,
                            ),
						),
						
						
						
												
						
                    )
                ) );	
				
				
				 Redux::setSection( $opt_name, array(
                    'icon'   => 'el-icon-th-large',
                    'title'  => esc_attr__( 'Footer Options', 'amio' ),
                    'fields' => array(
					
					 array(
							'id' => 'theme-footer-info',
							'type' => 'info',
		                    'notice' => true,
		                    'style' => 'info',
							'title' => esc_attr__('Footer Options', 'amio'),
							'desc' => esc_attr__('', 'amio')
							
					  ),
					  
					  
					 
					 array(
							'id' => 'wr-enable-footer-logo',
							'type' => 'button_set',
							'title' => esc_attr__('Enable Footer Logo', 'amio'),
							'subtitle' => esc_attr__('', 'amio'),
							'compiler' => 'true',
							'options' => array(
									'st1'=> esc_attr__('Enable', 'amio'),
									'st2' => esc_attr__('Disable', 'amio')
					
							),
							'default'  => 'st2'
					),
					
					array(
							'id' => 'footerlogopic',
							'type' => 'media',
							'compiler' => 'true',
							'title' => esc_attr__('Upload Logo', 'amio'),
							'subtitle' => esc_attr__('', 'amio'),
							'required' => array('wr-enable-footer-logo', '=' , 'st1')
					),
					
					$fields = array(
					'id'       => 'opt_footerlogo_dimensions',
					'type'     => 'dimensions',
					'units'    => array('em','px','%'),
					'output' => array('.footer-logo img'),
					'title'    => __('Logo Size', 'amio'),
					'subtitle' => __('', 'amio'),
					'desc'     => __('Optional', 'amio'),
					'default'  => array(
						'Width'   => '', 
						'Height'  => ''
					),
					'required' => array('wr-enable-footer-logo', '=' , 'st1')
				),
					
				array(
							'id' => 'theme-cus-copy',
							'type' => 'info',
		                    'notice' => true,
		                    'style' => 'info',
							'title' => esc_attr__('Copy right Text', 'amio'),
							'desc' => esc_attr__('Footer copy right Text', 'amio'),
							
							
					  ),
					
					array(
							'id' => 'copyright',
							'type' => 'textarea',
							'wpautop'=>true,
							'compiler' => 'true',
							'title' => esc_attr__('Copyright text of the WebSite', 'amio'),
							'subtitle' => esc_attr__('Write a Copyright text of your WebSite', 'amio'),
							'default'          => '&copy; 2017 AMIO. ALL RIGHTS RESERVED',
							'args'   => array(
								'teeny'            => true,
								'textarea_rows'    => 10
							),
							
					),
						
					
					)
                ) );
				
				
    
   
    /*
     * <--- END SECTIONS
     */


    /*
     *
     * YOU MUST PREFIX THE FUNCTIONS BELOW AND ACTION FUNCTION CALLS OR ANY OTHER CONFIG MAY OVERRIDE YOUR CODE.
     *
     */

    /*
    *
    * --> Action hook examples
    *
    */

    // If Redux is running as a plugin, this will remove the demo notice and links
    //add_action( 'redux/loaded', 'remove_demo' );

    // Function to test the compiler hook and demo CSS output.
    // Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
    //add_filter('redux/options/' . $opt_name . '/compiler', 'compiler_action', 10, 3);

    // Change the arguments after they've been declared, but before the panel is created
    //add_filter('redux/options/' . $opt_name . '/args', 'change_arguments' );

    // Change the default value of a field after it's been set, but before it's been useds
    //add_filter('redux/options/' . $opt_name . '/defaults', 'change_defaults' );

    // Dynamically add a section. Can be also used to modify sections/fields
    //add_filter('redux/options/' . $opt_name . '/sections', 'dynamic_section');

    /**
     * This is a test function that will let you see when the compiler hook occurs.
     * It only runs if a field    set with compiler=>true is changed.
     * */
    if ( ! function_exists( 'compiler_action' ) ) {
        function compiler_action( $options, $css, $changed_values ) {
            echo '<h1>The compiler hook has run!</h1>';
            echo "<pre>";
            print_r( $changed_values ); // Values that have changed since the last save
            echo "</pre>";
            //print_r($options); //Option values
            //print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )
        }
    }

    /**
     * Custom function for the callback validation referenced above
     * */
    if ( ! function_exists( 'redux_validate_callback_function' ) ) {
        function redux_validate_callback_function( $field, $value, $existing_value ) {
            $error   = false;
            $warning = false;

            //do your validation
            if ( $value == 1 ) {
                $error = true;
                $value = $existing_value;
            } elseif ( $value == 2 ) {
                $warning = true;
                $value   = $existing_value;
            }

            $return['value'] = $value;

            if ( $error == true ) {
                $return['error'] = $field;
                $field['msg']    = 'your custom error message';
            }

            if ( $warning == true ) {
                $return['warning'] = $field;
                $field['msg']      = 'your custom warning message';
            }

            return $return;
        }
    }

    /**
     * Custom function for the callback referenced above
     */
    if ( ! function_exists( 'redux_my_custom_field' ) ) {
        function redux_my_custom_field( $field, $value ) {
            print_r( $field );
            echo '<br/>';
            print_r( $value );
        }
    }

    /**
     * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
     * Simply include this function in the child themes functions.php file.
     * NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
     * so you must use get_template_directory_uri() if you want to use any of the built in icons
     * */
    if ( ! function_exists( 'dynamic_section' ) ) {
        function dynamic_section( $sections ) {
            //$sections = array();
            $sections[] = array(
                'title'  => __( 'Section via hook', 'amio' ),
                'desc'   => __( '<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'amio' ),
                'icon'   => 'el el-paper-clip',
                // Leave this as a blank section, no options just some intro text set above.
                'fields' => array()
            );

            return $sections;
        }
    }

    /**
     * Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
     * */
    if ( ! function_exists( 'change_arguments' ) ) {
        function change_arguments( $args ) {
            //$args['dev_mode'] = true;

            return $args;
        }
    }

    /**
     * Filter hook for filtering the default value of any given field. Very useful in development mode.
     * */
    if ( ! function_exists( 'change_defaults' ) ) {
        function change_defaults( $defaults ) {
            $defaults['str_replace'] = 'Testing filter hook!';

            return $defaults;
        }
    }

    /**
     * Removes the demo link and the notice of integrated demo from the redux-framework plugin
     */
    if ( ! function_exists( 'remove_demo' ) ) {
        function remove_demo() {
            // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
            if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
                remove_filter( 'plugin_row_meta', array(
                    ReduxFrameworkPlugin::instance(),
                    'plugin_metalinks'
                ), null, 2 );

                // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
            }
        }
    }

