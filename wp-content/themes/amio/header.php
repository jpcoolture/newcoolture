<?php defined('ABSPATH') or die("No script kiddies please!");?>
<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>

    <head>

        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <?php $amio_options = get_option('amio'); ?> 
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
        
	<?php 
	wp_head(); 
	?>
</head>
<body <?php body_class(); ?>>
<?php if (Amio_AfterSetupTheme::return_thme_option('wr-loading-page')=='st1'){?>
		<!-- Loading Screen -->
		   <div id="loader-wrapper">
			
			<!-- Loading Image -->
			<div class="loader-img"><img src="<?php echo esc_url(Amio_AfterSetupTheme::return_thme_option('loaderlogopic','url'));?>" alt="<?php esc_attr_e('Upload Logo','amio');?>" /></div>
			<!-- END Loading Image -->
			
			
			<!-- Loading Screen Split -->
			<div class="loader-section section-left"></div>
            <div class="loader-section section-right"></div>
			<!-- End Loading Screen Split -->
			
		</div> 
		<!-- End Loading Screen -->
		<?php } else{ ?>
		<?php } ;?>
<!-- Main Container -->
<div class="container">
<!-- All Sections -->
<div class="sections">
<?php get_template_part('template-parts/before-menu-section');?>