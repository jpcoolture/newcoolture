<?php defined('ABSPATH') or die("No script kiddies please!");?>
<?php $amio_options = get_option('amio'); ?> 
<?php
get_header();
 ?>
<?php if (class_exists('WooCommerce')) { ?>
<ul class="nav navbar-nav f-small normal woo-nab-ico raleway"><li><a href="<?php echo $woocommerce->cart->get_cart_url(); ?>"><i class="ion-bag f-normal"></i></a></li></ul>
<?php } ?>
<?php get_template_part('template-parts/after-menu-section');?>


       
         <?php if ( is_singular( 'product' ) ) {
           woocommerce_content();
          }else{
          //For ANY product archive.
          //Product taxonomy, product search or /shop landing
           woocommerce_get_template( 'archive-product.php' );
          }?>
          
       
      
<?php get_footer(); ?>
