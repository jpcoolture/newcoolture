<?php
/*
Plugin Name: Amio Plugin
Plugin URI: http://webredox.net/
Description: Declares a plugin that will create Page Settins, VC addons & Custom Post Type
Version: 1.4
Author: WebRedox
Author URI: http://webredox.net/
License: GPLv2
*/

include plugin_dir_path( __FILE__ ).'metaboxes.php';


if( ! function_exists( 'portfolio_post_types' ) ) {
    function portfolio_post_types() {

        register_post_type(
            'portfolio',
            array(
                'labels' => array(
                    'name'          => __( 'Portfolio Post', 'portfolio' ),
                    'singular_name' => __( 'Portfolio', 'portfolio' ),
                    'add_new'       => __( 'Add New', 'portfolio' ),
                    'add_new_item'  => __( 'Add New Portfolio', 'portfolio' ),
                    'edit'          => __( 'Edit', 'portfolio' ),
                    'edit_item'     => __( 'Edit Portfolio', 'portfolio' ),
                    'new_item'      => __( 'New Portfolio', 'portfolio' ),
                    'view'          => __( 'View Portfolio', 'portfolio' ),
                    'view_item'     => __( 'View Portfolio', 'portfolio' ),
                    'search_items'  => __( 'Search Portfolio', 'portfolio' ),
                    'not_found'     => __( 'No Portfolio item found', 'portfolio' ),
                    'not_found_in_trash' => __( 'No portfolio item found in Trash', 'portfolio' ),
                    'parent'        => __( 'Parent Portfolio', 'portfolio' ),
                ),
                
                'description'       => __( 'Create a Portfolio.', 'portfolio' ),
                'public'            => true,
                'show_ui'           => true,
                'show_in_menu'          => true,
                'publicly_queryable'    => true,
				'capability_type' => 'post',
                'exclude_from_search'   => true,
                'menu_position'         => 6,
                'hierarchical'      => false,
                'query_var'         => true,
				'menu_icon' => 'dashicons-portfolio',
                'supports'  => array (
                    'title', //Text input field to create a post title.
                    'editor',
                    'thumbnail',
                    
                )
            )
        );
register_taxonomy('portfolio_category', 'portfolio', array('hierarchical' => true, 'label' => 'Portfolio Categories', 'singular_name' => 'Category', "rewrite" => true, "query_var" => true));
        
        

    }
}

add_action( 'init', 'portfolio_post_types' ); // register post type

register_taxonomy_for_object_type('category', 'custom-type');

/**
*
*
*
 * Allow shortcodes in widgets
 * @since v1.0
 */
add_filter('widget_text', 'do_shortcode');

if( !function_exists('symple_fix_shortcodes') ) {
	function symple_fix_shortcodes($content){   
		$array = array (
			'<p>['		=> '[', 
			']</p>'		=> ']', 
			']<br />'	=> ']'
		);
		$content = strtr($content, $array);
		return $content;
	}
	add_filter('the_content', 'symple_fix_shortcodes');
}



// portfolio col 2
if(! function_exists('amio_portfolio2_shortcode')){
	function amio_portfolio2_shortcode($atts, $content = null){
		extract(shortcode_atts( array(
			'class'=>'',
			'id'=> '',
			'title'=> '',
			'postcount'=>'-1',
			'categoryname'=>'',
			'posttypes'=>'',
			'portstyle'=>'',
			'postoffset'=>'',
			'filter'=>'',
			
			
			), $atts) );
			

		$html ='';
		$dot ="'";
		$html .= '<div id="portfolio">';
			if($portstyle == "st2"){
			$html .= '<div class="section-content-full">';
			}
			else {
			$html .= '<div class="section-content">';
			}
			if($filter == "st2"){
			}
			else {
				if(!get_post_meta(get_the_ID(), 'portfolio_category', true)):
				$portfolio_category = get_terms('portfolio_category');
				if($portfolio_category):
				$html .= '<!-- Portfolio Filters -->';
				if($portstyle == "st2"){
				$html .= '<ul id="work" class="works-filter-full uk-subnav uppercase ultrabold">';
				}
				else {
				$html .= '<ul id="work" class="works-filter uk-subnav uppercase ultrabold">';
				}
				$html .= '<li data-uk-filter="" data-uk-scrollspy="{cls:'.$dot.'uk-animation-fade'.$dot.', delay:20}"><a href="">All</a></li>';
				foreach($portfolio_category as $portfolio_cat):
				$html .= '<li data-uk-filter="';
				$html .= $portfolio_cat->slug;
				$html .= '" data-uk-scrollspy="{cls:'.$dot.'uk-animation-fade'.$dot.', delay:40}"><a href="">';
				$html .= $portfolio_cat->name;
				$html .= '</a></li>';
				endforeach;
				$html .= '</ul>';
				$html .= '<!-- End Portfolio Filters -->';
				endif; 
				endif;
				}
				$html .= '<!-- Work Previews -->';
				$html .= '<div data-uk-grid="{controls: '.$dot.'#work'.$dot.' }">';
				global $post;
				$paged=(get_query_var('paged'))?get_query_var('paged'):1;
				$loop = new WP_Query( array( 'post_type' => 'portfolio', 'portfolio_category'=> $categoryname, 'posts_per_page'=> $postcount, 'orderby' => 'post_date', 'showposts' => $postcount, 'offset' => $postoffset ) );
				while ( $loop->have_posts() ) : $loop->the_post();
				$portfolio_categorys = wp_get_post_terms($post->ID,'portfolio_category');
				$amio_class = ""; 
				$amio_cat = "";
				foreach ($portfolio_categorys as $item) {
				$amio_class.=esc_attr($item->slug . ',');
				$amio_cat.= $item->name . ' ';	
				}
				if (has_post_thumbnail( $post->ID ) ):
		        $amio_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' );
				$html .= '<!-- Work -->';
					$html .= '<div class="work uk-width-small-1-1 uk-width-medium-1-2" '; 		$html .= 'data-uk-filter="';
					$html .= ''.$amio_class.'';
					$html .= '">';
					$html .='<img src="';
					$html .= $amio_image[0];
					$html .='" alt="">';
					if(get_post_meta($post->ID,'rnr_port_click_opt',true)=='st2'){
					$html .= '<a href="';
					$html .= $amio_image[0];
					$html .= '" data-lightbox="works" title="';
					$html .= get_the_title();
					$html .= '">';
					}
					else {
					$html .= '<a href="';
					$html .= get_the_permalink();
					$html .= '" title="';
					$html .= get_the_title();
					$html .= '">';
					}
						$html .= '<div class="hover">';
						$html .= '<p class="dark f-medium normal uppercase">';
						$html .= get_the_title();
						$html .= '</p>';
						$html .= '</div>';
					$html .= '</a>';
					$html .= '</div>';
				$html .= '<!-- End Work -->';
				endif;
				endwhile;
				wp_reset_postdata();
				$html .= '</div>';
				$html .= '<!-- End Work Previews -->';
			$html .= '</div>';
		$html .= '</div>';
		
		
				
		return $html ;
	}
	add_shortcode('amio_portfolio_two_col', 'amio_portfolio2_shortcode');
}

// portfolio col 3
if(! function_exists('amio_portfolio3_shortcode')){
	function amio_portfolio3_shortcode($atts, $content = null){
		extract(shortcode_atts( array(
			'class'=>'',
			'id'=> '',
			'title'=> '',
			'postcount'=>'-1',
			'categoryname'=>'',
			'posttypes'=>'',
			'portstyle'=>'',
			'portpoststyle'=>'',
			'postoffset'=>'',
			'filter'=>'st1',
			
			
			), $atts) );
			

		$html ='';
		$dot ="'";
		$gutter ="";
		if($portpoststyle == "st2"){
		$gutter .='gutter';
		}
		$html .= '<div id="portfolio">';
			if($portstyle == "st2"){
			$html .= '<div class="section-content-full">';
			}
			else {
			$html .= '<div class="section-content">';
			}
			if($filter == "st2"){
			}
			else {
				if(!get_post_meta(get_the_ID(), 'portfolio_category', true)):
				$portfolio_category = get_terms('portfolio_category');
				if($portfolio_category):
				$html .= '<!-- Portfolio Filters -->';
				if($portstyle == "st2"){
				$html .= '<ul id="work" class="works-filter-full  uk-subnav uppercase ultrabold">';
				}
				else {
				$html .= '<ul id="work" class="works-filter '.$gutter.' uk-subnav uppercase ultrabold">';
				}
				$html .= '<li data-uk-filter="" data-uk-scrollspy="{cls:'.$dot.'uk-animation-fade'.$dot.', delay:20}"><a href="">All</a></li>';
				foreach($portfolio_category as $portfolio_cat):
				$html .= '<li data-uk-filter="';
				$html .= $portfolio_cat->slug;
				$html .= '" data-uk-scrollspy="{cls:'.$dot.'uk-animation-fade'.$dot.', delay:40}"><a href="">';
				$html .= $portfolio_cat->name;
				$html .= '</a></li>';
				endforeach;
				$html .= '</ul>';
				$html .= '<!-- End Portfolio Filters -->';
				endif; 
				endif;
				}
				$html .= '<!-- Work Previews -->';
				$html .= '<div data-uk-grid="{controls: '.$dot.'#work'.$dot.' }">';
				global $post;
				$paged=(get_query_var('paged'))?get_query_var('paged'):1;
				$loop = new WP_Query( array( 'post_type' => 'portfolio', 'portfolio_category'=> $categoryname, 'posts_per_page'=> $postcount, 'orderby' => 'post_date', 'offset' => $postoffset ) );
				while ( $loop->have_posts() ) : $loop->the_post();
				$portfolio_categorys = wp_get_post_terms($post->ID,'portfolio_category');
				$amio_class = ""; 
				$amio_cat = "";
				foreach ($portfolio_categorys as $item) {
				$amio_class.=esc_attr($item->slug . ',');
				$amio_cat.= $item->name . ' ';	
				}
				if (has_post_thumbnail( $post->ID ) ):
		        $amio_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' );
				$html .= '<!-- Work -->';
					$html .= '<div class="work '.$gutter.' uk-width-small-1-2 uk-width-medium-1-3" '; 		
					$html .= 'data-uk-filter="';
					$html .= ''.$amio_class.'';
					$html .= '">';
					$html .='<img src="';
					$html .= $amio_image[0];
					$html .='" alt="">';
					if(get_post_meta($post->ID,'rnr_port_click_opt',true)=='st2'){
					$html .= '<a href="';
					$html .= $amio_image[0];
					$html .= '" data-lightbox="works" title="';
					$html .= get_the_title();
					$html .= '">';
					}
					else {
					$html .= '<a href="';
					$html .= get_the_permalink();
					$html .= '" title="';
					$html .= get_the_title();
					$html .= '">';
					}
						$html .= '<div class="hover">';
						$html .= '<p class="dark f-medium normal uppercase">';
						$html .= get_the_title();
						$html .= '</p>';
						$html .= '</div>';
					$html .= '</a>';
					$html .= '</div>';
				$html .= '<!-- End Work -->';
				endif;
				endwhile;
				wp_reset_postdata();
				$html .= '</div>';
				$html .= '<!-- End Work Previews -->';
			$html .= '</div>';
		$html .= '</div>';
		
		
				
		return $html ;
	}
	add_shortcode('amio_portfolio_three_col', 'amio_portfolio3_shortcode');
}

// portfolio col 4
if(! function_exists('amio_portfolio4_shortcode')){
	function amio_portfolio4_shortcode($atts, $content = null){
		extract(shortcode_atts( array(
			'class'=>'',
			'id'=> '',
			'title'=> '',
			'postcount'=>'-1',
			'categoryname'=>'',
			'posttypes'=>'',
			'portstyle'=>'',
			'portpoststyle'=>'',
			'postoffset'=>'',
			'filter'=>'',
			
			
			), $atts) );
			

		$html ='';
		$dot ="'";
		$gutter ="";
		if($portpoststyle == "st2"){
		$gutter .='gutter';
		}
		$html .= '<div id="portfolio">';
			if($portstyle == "st2"){
			$html .= '<div class="section-content-full">';
			}
			else {
			$html .= '<div class="section-content">';
			}
			if($filter == "st2"){
			}
			else {
				if(!get_post_meta(get_the_ID(), 'portfolio_category', true)):
				$portfolio_category = get_terms('portfolio_category');
				if($portfolio_category):
				$html .= '<!-- Portfolio Filters -->';
				if($portstyle == "st2"){
				$html .= '<ul id="work" class="works-filter-full  uk-subnav uppercase ultrabold">';
				}
				else {
				$html .= '<ul id="work" class="works-filter '.$gutter.' uk-subnav uppercase ultrabold">';
				}
				$html .= '<li data-uk-filter="" data-uk-scrollspy="{cls:'.$dot.'uk-animation-fade'.$dot.', delay:20}"><a href="">All</a></li>';
				foreach($portfolio_category as $portfolio_cat):
				$html .= '<li data-uk-filter="';
				$html .= $portfolio_cat->slug;
				$html .= '" data-uk-scrollspy="{cls:'.$dot.'uk-animation-fade'.$dot.', delay:40}"><a href="">';
				$html .= $portfolio_cat->name;
				$html .= '</a></li>';
				endforeach;
				$html .= '</ul>';
				$html .= '<!-- End Portfolio Filters -->';
				endif; 
				endif;
				}
				$html .= '<!-- Work Previews -->';
				$html .= '<div data-uk-grid="{controls: '.$dot.'#work'.$dot.' }">';
				global $post;
				$paged=(get_query_var('paged'))?get_query_var('paged'):1;
				$loop = new WP_Query( array( 'post_type' => 'portfolio', 'portfolio_category'=> $categoryname, 'posts_per_page'=> $postcount, 'orderby' => 'post_date', 'showposts' => $postcount, 'offset' => $postoffset ) );
				while ( $loop->have_posts() ) : $loop->the_post();
				$portfolio_categorys = wp_get_post_terms($post->ID,'portfolio_category');
				$amio_class = ""; 
				$amio_cat = "";
				foreach ($portfolio_categorys as $item) {
				$amio_class.=esc_attr($item->slug . ',');
				$amio_cat.= $item->name . ' ';	
				}
				if (has_post_thumbnail( $post->ID ) ):
		        $amio_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' );
				$html .= '<!-- Work -->';
					$html .= '<div class="work '.$gutter.' uk-width-small-1-2 uk-width-medium-1-4" '; 		
					$html .= 'data-uk-filter="';
					$html .= ''.$amio_class.'';
					$html .= '">';
					$html .='<img src="';
					$html .= $amio_image[0];
					$html .='" alt="">';
					if(get_post_meta($post->ID,'rnr_port_click_opt',true)=='st2'){
					$html .= '<a href="';
					$html .= $amio_image[0];
					$html .= '" data-lightbox="works" title="';
					$html .= get_the_title();
					$html .= '">';
					}
					else {
					$html .= '<a href="';
					$html .= get_the_permalink();
					$html .= '" title="';
					$html .= get_the_title();
					$html .= '">';
					}
						$html .= '<div class="hover">';
						$html .= '<p class="dark f-medium normal uppercase">';
						$html .= get_the_title();
						$html .= '</p>';
						$html .= '</div>';
					$html .= '</a>';
					$html .= '</div>';
				$html .= '<!-- End Work -->';
				endif;
				endwhile;
				wp_reset_postdata();
				$html .= '</div>';
				$html .= '<!-- End Work Previews -->';
			$html .= '</div>';
		$html .= '</div>';
		
		
				
		return $html ;
	}
	add_shortcode('amio_portfolio_four_col', 'amio_portfolio4_shortcode');
}

// portfolio col 5
if(! function_exists('amio_portfolio5_shortcode')){
	function amio_portfolio5_shortcode($atts, $content = null){
		extract(shortcode_atts( array(
			'class'=>'',
			'id'=> '',
			'title'=> '',
			'postcount'=>'-1',
			'categoryname'=>'',
			'posttypes'=>'',
			'portstyle'=>'',
			'portpoststyle'=>'',
			'postoffset'=>'',
			'filter'=>'',
			
			
			), $atts) );
			

		$html ='';
		$dot ="'";
		$gutter ="";
		if($portpoststyle == "st2"){
		$gutter .='gutter';
		}
		$html .= '<div id="portfolio">';
			if($portstyle == "st2"){
			$html .= '<div class="section-content-full">';
			}
			else {
			$html .= '<div class="section-content">';
			}
			if($filter == "st2"){
			}
			else {
				if(!get_post_meta(get_the_ID(), 'portfolio_category', true)):
				$portfolio_category = get_terms('portfolio_category');
				if($portfolio_category):
				$html .= '<!-- Portfolio Filters -->';
				if($portstyle == "st2"){
				$html .= '<ul id="work" class="works-filter-full  uk-subnav uppercase ultrabold">';
				}
				else {
				$html .= '<ul id="work" class="works-filter '.$gutter.' uk-subnav uppercase ultrabold">';
				}
				$html .= '<li data-uk-filter="" data-uk-scrollspy="{cls:'.$dot.'uk-animation-fade'.$dot.', delay:20}"><a href="">All</a></li>';
				foreach($portfolio_category as $portfolio_cat):
				$html .= '<li data-uk-filter="';
				$html .= $portfolio_cat->slug;
				$html .= '" data-uk-scrollspy="{cls:'.$dot.'uk-animation-fade'.$dot.', delay:40}"><a href="">';
				$html .= $portfolio_cat->name;
				$html .= '</a></li>';
				endforeach;
				$html .= '</ul>';
				$html .= '<!-- End Portfolio Filters -->';
				endif; 
				endif;
				}
				$html .= '<!-- Work Previews -->';
				$html .= '<div data-uk-grid="{controls: '.$dot.'#work'.$dot.' }">';
				
				global $post;
				$paged=(get_query_var('paged'))?get_query_var('paged'):1;
				$loop = new WP_Query( array( 'post_type' => 'portfolio', 'portfolio_category'=> $categoryname, 'posts_per_page'=> $postcount, 'orderby' => 'post_date', 'showposts' => $postcount, 'offset' => $postoffset ) );
				while ( $loop->have_posts() ) : $loop->the_post();
				$portfolio_categorys = wp_get_post_terms($post->ID,'portfolio_category');
				$amio_class = ""; 
				$amio_cat = "";
				foreach ($portfolio_categorys as $item) {
				$amio_class.=esc_attr($item->slug . ',');
				$amio_cat.= $item->name . ' ';	
				}
				if (has_post_thumbnail( $post->ID ) ):
		        $amio_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' );
				$html .= '<!-- Work -->';
					$html .= '<div class="work '.$gutter.' uk-width-small-1-2 uk-width-medium-1-5" '; 		
					$html .= 'data-uk-filter="';
					$html .= ''.$amio_class.'';
					$html .= '">';
					$html .='<img src="';
					$html .= $amio_image[0];
					$html .='" alt="">';
					if(get_post_meta($post->ID,'rnr_port_click_opt',true)=='st2'){
					$html .= '<a href="';
					$html .= $amio_image[0];
					$html .= '" data-lightbox="works" title="';
					$html .= get_the_title();
					$html .= '">';
					}
					else {
					$html .= '<a href="';
					$html .= get_the_permalink();
					$html .= '" title="';
					$html .= get_the_title();
					$html .= '">';
					}
						$html .= '<div class="hover">';
						$html .= '<p class="dark f-medium normal uppercase">';
						$html .= get_the_title();
						$html .= '</p>';
						$html .= '</div>';
					$html .= '</a>';
					$html .= '</div>';
				$html .= '<!-- End Work -->';
				endif;
				endwhile;
				wp_reset_postdata();
				$html .= '</div>';
				$html .= '<!-- End Work Previews -->';
			$html .= '</div>';
		$html .= '</div>';
		
		
				
		return $html ;
	}
	add_shortcode('amio_portfolio_five_col', 'amio_portfolio5_shortcode');
}

// portfolio full
if(! function_exists('amio_portfolio_full_shortcode')){
	function amio_portfolio_full_shortcode($atts, $content = null){
		extract(shortcode_atts( array(
			'class'=>'',
			'id'=> '',
			'title'=> '',
			'postcount'=>'-1',
			'categoryname'=>'',
			'posttypes'=>'',
			'portstyle'=>'',
			'postoffset'=>'',
			'filter'=>'',
			
			
			), $atts) );
			

		$html ='';
		$dot ="'";
		$html .= '<div id="portfolio">';
			if($portstyle == "st2"){
			$html .= '<div class="section-content-full">';
			}
			else {
			$html .= '<div class="section-content">';
			}
			if($filter == "st2"){
			}
			else {
				if(!get_post_meta(get_the_ID(), 'portfolio_category', true)):
				$portfolio_category = get_terms('portfolio_category');
				if($portfolio_category):
				$html .= '<!-- Portfolio Filters -->';
				if($portstyle == "st2"){
				$html .= '<ul id="work" class="works-filter-full uk-subnav uppercase ultrabold">';
				}
				else {
				$html .= '<ul id="work" class="works-filter uk-subnav uppercase ultrabold">';
				}
				$html .= '<li data-uk-filter="" data-uk-scrollspy="{cls:'.$dot.'uk-animation-fade'.$dot.', delay:20}"><a href="">All</a></li>';
				foreach($portfolio_category as $portfolio_cat):
				$html .= '<li data-uk-filter="';
				$html .= $portfolio_cat->slug;
				$html .= '" data-uk-scrollspy="{cls:'.$dot.'uk-animation-fade'.$dot.', delay:40}"><a href="">';
				$html .= $portfolio_cat->name;
				$html .= '</a></li>';
				endforeach;
				$html .= '</ul>';
				$html .= '<!-- End Portfolio Filters -->';
				endif; 
				endif;
				}
				$html .= '<!-- Work Previews -->';
				$html .= '<div data-uk-grid="{controls: '.$dot.'#work'.$dot.' }">';
				global $post;
				$paged=(get_query_var('paged'))?get_query_var('paged'):1;
				$loop = new WP_Query( array( 'post_type' => 'portfolio', 'portfolio_category'=> $categoryname, 'posts_per_page'=> $postcount, 'orderby' => 'post_date', 'showposts' => $postcount, 'offset' => $postoffset ) );
				while ( $loop->have_posts() ) : $loop->the_post();
				$portfolio_categorys = wp_get_post_terms($post->ID,'portfolio_category');
				$amio_class = ""; 
				$amio_cat = "";
				foreach ($portfolio_categorys as $item) {
				$amio_class.=esc_attr($item->slug . ',');
				$amio_cat.= $item->name . ' ';	
				}
				if (has_post_thumbnail( $post->ID ) ):
		        $amio_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' );
				$html .= '<!-- Work -->';
					$html .= '<div class="work uk-width-small-1-1 uk-width-medium-1-1" '; 		$html .= 'data-uk-filter="';
					$html .= ''.$amio_class.'';
					$html .= '">';
					$html .='<img src="';
					$html .= $amio_image[0];
					$html .='" alt="" class="full-width">';
					if(get_post_meta($post->ID,'rnr_port_click_opt',true)=='st2'){
					$html .= '<a href="';
					$html .= $amio_image[0];
					$html .= '" data-lightbox="works" title="';
					$html .= get_the_title();
					$html .= '">';
					}
					else {
					$html .= '<a href="';
					$html .= get_the_permalink();
					$html .= '" title="';
					$html .= get_the_title();
					$html .= '">';
					}
						$html .= '<div class="hover">';
						$html .= '<p class="dark f-medium normal uppercase">';
						$html .= get_the_title();
						$html .= '</p>';
						$html .= '</div>';
					$html .= '</a>';
					$html .= '</div>';
				$html .= '<!-- End Work -->';
				endif;
				endwhile;
				wp_reset_postdata();
				$html .= '</div>';
				$html .= '<!-- End Work Previews -->';
			$html .= '</div>';
		$html .= '</div>';
		
		
				
		return $html ;
	}
	add_shortcode('amio_portfolio_full', 'amio_portfolio_full_shortcode');
}

//feature box

if(! function_exists('amio_feature_shortcode')){
	function amio_feature_shortcode($atts, $content = null){
		extract(shortcode_atts( array(
			'class'=>'',
			'link_target'=>'',
			'buttontext'=>'',
			'buttonurl'=>'',
			'title'=>'',
			'subtitle'=>'',
			'featurestyle'=>'',
			
			
			), $atts) );
			
		$html='';
		$dot ="'";
		$html .='<div class="service" data-uk-scrollspy="{cls:'.$dot.'uk-animation-fade'.$dot.', delay:20}">';
		$html .='<i class="icon '.$class.' float-l"></i>
		  <h2 class="ultrabold uppercase float-l">'.$title.'</h2> <div class="separator-small float-l"></div>
		  <p class="float-l t-left">'.$content.' </p>';
		$html .='</div>';
		
				
				
		return $html;
	}
	add_shortcode('amio_service', 'amio_feature_shortcode');
}

//contact info

if(! function_exists('amio_contact_form_shortcode')){
	function amio_contact_form_shortcode($atts, $content = null){
		extract(shortcode_atts( array(
			'class'=>'',
			'id'=>'',
			'image'=>'',
			'conform'=>'',
			
			
			
			
			
			), $atts) );
			
		$html='';
		
		
		$html .= do_shortcode($conform);
	
		
				
		return $html;
	}
	add_shortcode('amio_contact_form', 'amio_contact_form_shortcode');
}


?>