<?php if (get_post_meta($post->ID,'rnr_bl-video',true)!=''):?>
<!-- Begin Vimeo embed video -->
	<div class="embed-responsive embed-responsive-16by9">
		<iframe src="<?php echo esc_url(get_post_meta($post->ID,'rnr_bl-video',true));?>" allowfullscreen=""></iframe>
	</div>
<!-- End Vimeo embed video -->
<br>
<?php endif;?>
										