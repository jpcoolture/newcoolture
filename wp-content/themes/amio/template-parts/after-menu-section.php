<?php defined('ABSPATH') or die("No script kiddies please!");?>
<?php $amio_options = get_option('amio'); ?>

						
							<!-- Navigation List -->
							
								<?php

										$defaults = array(
													'theme_location'  => 'top-menu',
													'menu'            => 'nav',
													'container'       => '',
													'container_class' => '',
													'menu_class'      => 'navbar-main-menu',
													'menu_id'         => '',
													'echo'            => true,
													'fallback_cb'     => 'wp_page_menu',
													'before'          => '',
													'after'           => '',
													'link_before'     => '',
													'link_after'      => '',
													'items_wrap'      => '<ul class="nav navbar-nav f-small normal raleway %2$s">%3$s</ul>',
													'depth'           => 0,
													
														);
								if(has_nav_menu('top-menu')) {
														wp_nav_menu( $defaults );
								}
										  else {
											echo '<ul class="nav navbar-nav f-small normal raleway"><li><a>No menu assigned!</a></li></ul>';
										  }
														?>
							
							<!-- End Navigation List -->	
							
						 
						</div>
						<!-- End Navigation Links -->
						
						
					</div>
					<!-- Navigation Ends -->
				
				</div>
				<!-- End Sticky Navigation -->