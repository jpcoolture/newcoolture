<?php
/*** Removing shortcodes ***/
vc_remove_element("vc_widget_sidebar");
vc_remove_element("vc_gallery");
vc_remove_element("vc_wp_search");
vc_remove_element("vc_wp_meta");
vc_remove_element("vc_wp_recentcomments");
vc_remove_element("vc_wp_calendar");
vc_remove_element("vc_wp_pages");
vc_remove_element("vc_wp_tagcloud");
vc_remove_element("vc_wp_custommenu");
vc_remove_element("vc_wp_text");
vc_remove_element("vc_wp_posts");
vc_remove_element("vc_wp_links");
vc_remove_element("vc_wp_categories");
vc_remove_element("vc_wp_archives");
vc_remove_element("vc_wp_rss");
vc_remove_element("vc_teaser_grid");
vc_remove_element("vc_button");
vc_remove_element("vc_button2");
vc_remove_element("vc_cta_button");
vc_remove_element("vc_cta_button2");
vc_remove_element("vc_message");
vc_remove_element("vc_tour");
vc_remove_element("vc_progress_bar");
vc_remove_element("vc_pie");
vc_remove_element("vc_posts_slider");
vc_remove_element("vc_toggle");
vc_remove_element("vc_images_carousel");
vc_remove_element("vc_posts_grid");
vc_remove_element("vc_carousel");

/*** Remove unused parameters ***/
if (function_exists('vc_remove_param')) {
	
	vc_remove_param('vc_row', 'bg_image');
	vc_remove_param('vc_row', 'bg_color');
	vc_remove_param('vc_row', 'font_color');
	vc_remove_param('vc_row', 'margin_bottom');
	vc_remove_param('vc_row', 'bg_image_repeat');
	vc_remove_param('vc_tabs', 'interval');
	vc_remove_param('vc_separator', 'style');
	vc_remove_param('vc_separator', 'color');
	vc_remove_param('vc_separator', 'accent_color');
	vc_remove_param('vc_separator', 'el_width');
	vc_remove_param('vc_text_separator', 'style');
	vc_remove_param('vc_text_separator', 'color');
	vc_remove_param('vc_text_separator', 'accent_color');
	vc_remove_param('vc_text_separator', 'el_width');
}

/*** Remove frontend editor ***/
if(function_exists('vc_disable_frontend')){
	vc_disable_frontend();
}


/***************** Portfolio Shortcodes *********************/
//Portfolio 1 col
vc_map( array(
		"name" => esc_attr__('Portfolio 1 Column', 'amio'),
		"base" => "amio_portfolio_full",
		"category" => esc_attr__('Portfolio Element', 'amio'),
		"icon" => "icon-wpb-team",
		"allowed_container_element" => 'vc_row',
		"params" => array(
			
			
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => esc_attr__('Element Style', 'amio'),
				"param_name" => "portstyle",
				"value" => array(
					"Boxed" => "st1",
					"Full Width" => "st2",
					
					
				),
				"description" => "",
			),
			
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => esc_attr__('Filter Option', 'freebird'),
				"param_name" => "filter",
				"value" => array(
					"Enable" => "st1",
					"Disable" => "st2",
					
										
				),
				"description" => "",
			),
			
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => "Category Name",
				"param_name" => "categoryname",
				"value" => "",
				"description" => "Use this field if you need.",
				"dependency" => Array('element' => "filter", 'value' => array('st2'))
			),
			
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => esc_attr__('Post Count', 'amio'),
				"param_name" => "postcount",
				"value" => "",
				"description" => esc_attr__('Use this field if you need.', 'amio'),
			),
			
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => esc_attr__('Post Offset', 'amio'),
				"param_name" => "postoffset",
				"value" => "",
				"description" => esc_attr__('Use this field if you need.', 'amio'),
			),	

			
			
		)
) );

//Portfolio 2 col
vc_map( array(
		"name" => esc_attr__('Portfolio 2 Column', 'amio'),
		"base" => "amio_portfolio_two_col",
		"category" => esc_attr__('Portfolio Element', 'amio'),
		"icon" => "icon-wpb-team",
		"allowed_container_element" => 'vc_row',
		"params" => array(
			
			
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => esc_attr__('Element Style', 'amio'),
				"param_name" => "portstyle",
				"value" => array(
					"Boxed" => "st1",
					"Full Width" => "st2",
					
					
				),
				"description" => "",
			),
			
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => esc_attr__('Filter Option', 'freebird'),
				"param_name" => "filter",
				"value" => array(
					"Enable" => "st1",
					"Disable" => "st2",
					
										
				),
				"description" => "",
			),
			
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => "Category Name",
				"param_name" => "categoryname",
				"value" => "",
				"description" => "Use this field if you need.",
				"dependency" => Array('element' => "filter", 'value' => array('st2'))
			),
			
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => esc_attr__('Post Count', 'amio'),
				"param_name" => "postcount",
				"value" => "",
				"description" => esc_attr__('Use this field if you need.', 'amio'),
			),
			
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => esc_attr__('Post Offset', 'amio'),
				"param_name" => "postoffset",
				"value" => "",
				"description" => esc_attr__('Use this field if you need.', 'amio'),
			),	

			
			
		)
) );

//Portfolio 3 col
vc_map( array(
		"name" => esc_attr__('Portfolio 3 Column', 'amio'),
		"base" => "amio_portfolio_three_col",
		"category" => esc_attr__('Portfolio Element', 'amio'),
		"icon" => "icon-wpb-team",
		"allowed_container_element" => 'vc_row',
		"params" => array(
			
			
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => esc_attr__('Element Style', 'amio'),
				"param_name" => "portstyle",
				"value" => array(
					"Boxed" => "st1",
					"Full Width" => "st2",
					
					
				),
				"description" => "",
			),
			
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => esc_attr__('Post Style', 'amio'),
				"param_name" => "portpoststyle",
				"value" => array(
					"Default" => "st1",
					"Gutter" => "st2",
					
					
				),
				"description" => "",
			),
			
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => esc_attr__('Filter Option', 'freebird'),
				"param_name" => "filter",
				"value" => array(
					"Enable" => "st1",
					"Disable" => "st2",
					
										
				),
				"description" => "",
			),
			
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => "Category Name",
				"param_name" => "categoryname",
				"value" => "",
				"description" => "Use this field if you need.",
				"dependency" => Array('element' => "filter", 'value' => array('st2'))
			),
			
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => esc_attr__('Post Count', 'amio'),
				"param_name" => "postcount",
				"value" => "",
				"description" => esc_attr__('Use this field if you need.', 'amio'),
			),
			
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => esc_attr__('Post Offset', 'amio'),
				"param_name" => "postoffset",
				"value" => "",
				"description" => esc_attr__('Use this field if you need.', 'amio'),
			),	

			
			
		)
) );

//Portfolio 4 col
vc_map( array(
		"name" => esc_attr__('Portfolio 4 Column', 'amio'),
		"base" => "amio_portfolio_four_col",
		"category" => esc_attr__('Portfolio Element', 'amio'),
		"icon" => "icon-wpb-team",
		"allowed_container_element" => 'vc_row',
		"params" => array(
			
			
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => esc_attr__('Element Style', 'amio'),
				"param_name" => "portstyle",
				"value" => array(
					"Boxed" => "st1",
					"Full Width" => "st2",
					
					
				),
				"description" => "",
			),
			
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => esc_attr__('Post Style', 'amio'),
				"param_name" => "portpoststyle",
				"value" => array(
					"Default" => "st1",
					"Gutter" => "st2",
					
					
				),
				"description" => "",
			),
			
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => esc_attr__('Filter Option', 'freebird'),
				"param_name" => "filter",
				"value" => array(
					"Enable" => "st1",
					"Disable" => "st2",
					
										
				),
				"description" => "",
			),
			
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => "Category Name",
				"param_name" => "categoryname",
				"value" => "",
				"description" => "Use this field if you need.",
				"dependency" => Array('element' => "filter", 'value' => array('st2'))
			),
			
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => esc_attr__('Post Count', 'amio'),
				"param_name" => "postcount",
				"value" => "",
				"description" => esc_attr__('Use this field if you need.', 'amio'),
			),
			
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => esc_attr__('Post Offset', 'amio'),
				"param_name" => "postoffset",
				"value" => "",
				"description" => esc_attr__('Use this field if you need.', 'amio'),
			),	

			
			
		)
) );

//Portfolio 5 col
vc_map( array(
		"name" => esc_attr__('Portfolio 5 Column', 'amio'),
		"base" => "amio_portfolio_five_col",
		"category" => esc_attr__('Portfolio Element', 'amio'),
		"icon" => "icon-wpb-team",
		"allowed_container_element" => 'vc_row',
		"params" => array(
			
			
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => esc_attr__('Element Style', 'amio'),
				"param_name" => "portstyle",
				"value" => array(
					"Boxed" => "st1",
					"Full Width" => "st2",
					
					
				),
				"description" => "",
			),
			
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => esc_attr__('Post Style', 'amio'),
				"param_name" => "portpoststyle",
				"value" => array(
					"Default" => "st1",
					"Gutter" => "st2",
					
					
				),
				"description" => "",
			),
			
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => esc_attr__('Filter Option', 'freebird'),
				"param_name" => "filter",
				"value" => array(
					"Enable" => "st1",
					"Disable" => "st2",
					
										
				),
				"description" => "",
			),
			
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => "Category Name",
				"param_name" => "categoryname",
				"value" => "",
				"description" => "Use this field if you need.",
				"dependency" => Array('element' => "filter", 'value' => array('st2'))
			),
			
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => esc_attr__('Post Count', 'amio'),
				"param_name" => "postcount",
				"value" => "",
				"description" => esc_attr__('Use this field if you need.', 'amio'),
			),
			
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => esc_attr__('Post Offset', 'amio'),
				"param_name" => "postoffset",
				"value" => "",
				"description" => esc_attr__('Use this field if you need.', 'amio'),
			),	

			
			
		)
) );
/***************** Portfolio Shortcodes *********************/

//header 1
class WPBakeryShortCode_Wr_Vc_Head_Simples  extends WPBakeryShortCodesContainer {}
//Register "container" content element. It will hold all your inner (child) content elements
vc_map( array(
        "name" => "Header Style 1", "cinestar",
        "base" => "wr_vc_head_simples",
		"as_parent" => array('only' => 'wr_vc_header_one_title, wr_vc_header_one_subtitle,  wr_vc_profiles, wr_vc_single_profile'),
		"content_element" => true,
		"category" => 'Page Header',
		"icon" => "icon-wpb-qode_clients",
        "show_settings_on_create" => true,
        "params" => array(
			
			
			

        ),
        "js_view" => 'VcColumnView'
) );


// profile multi
class WPBakeryShortCode_WR_VC_Profiles  extends WPBakeryShortCodesContainer {}
//Register "container" content element. It will hold all your inner (child) content elements
vc_map( array(
        "name" => "Multiple Profile Picture", "cinestar",
        "base" => "wr_vc_profiles",
		"as_child" => array('only' => 'wr_vc_head_simple, wr_vc_head_two, wr_vc_head_three, wr_vc_head_four, wr_vc_head_five, wr_vc_head_six, wr_vc_head_seven, wr_vc_head_nine'),
        "as_parent" => array('only' => 'wr_vc_profile'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
		"content_element" => true,
		"category" => 'Page Header',
		"icon" => "icon-wpb-qode_clients",
        "show_settings_on_create" => true,
        "params" => array(
			
			
			

        ),
        "js_view" => 'VcColumnView'
) );

class WPBakeryShortCode_WR_VC_Profile extends WPBakeryShortCode {}
vc_map( array(
        "name" => "Profile Item", "cinestar",
        "base" => "wr_vc_profile",
        "content_element" => true,
		"icon" => "icon-wpb-qode_client",
        "as_child" => array('only' => 'wr_vc_profiles'), // Use only|except attributes to limit parent (separate multiple values with comma)
        "params" => array(
				
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => "Title",
				"param_name" => "title",
				"value" => ""
			), 
			
			array(
				"type" => "attach_image",
				"holder" => "div",
				"class" => "",
				"heading" => "Upload Image",
				"param_name" => "amio_img",
				"description" => "",
				
			),
			
			array(
				"type" => "attach_image",
				"holder" => "div",
				"class" => "",
				"heading" => "Upload Popup Image",
				"param_name" => "amio_pop_img",
				"description" => "",
				
			),
			
			

		
        )
) );

//profile
class WPBakeryShortCode_WR_VC_Single_Profile extends WPBakeryShortCode {}
vc_map( array(
        "name" => "Single Profile", "cinestar",
        "base" => "wr_vc_single_profile",
        "content_element" => true,
		"icon" => "icon-wpb-qode_client",
        "as_child" => array('only' => 'wr_vc_head_simple, wr_vc_head_two, wr_vc_head_three, wr_vc_head_four, wr_vc_head_five, wr_vc_head_six, wr_vc_head_seven, wr_vc_head_nine'), // Use only|except attributes to limit parent (separate multiple values with comma)
        "params" => array(
				
			
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => "Title",
				"param_name" => "title",
				"value" => ""
			), 
			
			array(
				"type" => "attach_image",
				"holder" => "div",
				"class" => "",
				"heading" => "Upload Image",
				"param_name" => "amio_img",
				"description" => "",
				
			),
			
			array(
				"type" => "attach_image",
				"holder" => "div",
				"class" => "",
				"heading" => "Upload Popup Image",
				"param_name" => "amio_pop_img",
				"description" => "",
				
			),
			
	
        )
) );

//title
class WPBakeryShortCode_WR_VC_Header_One_Title extends WPBakeryShortCode {}
vc_map( array(
        "name" => "Title", "cinestar",
        "base" => "wr_vc_header_one_title",
        "content_element" => true,
		"icon" => "icon-wpb-qode_client",
        "as_child" => array('only' => 'wr_vc_head_simple'), // Use only|except attributes to limit parent (separate multiple values with comma)
        "params" => array(
				
			
			array(
				"type" => "textarea_html",
				"holder" => "div",
				"class" => "",
				"heading" => "Title",
				"param_name" => "content",
				"value" => "",
				"description" => "Markup Text: Markup &lt;span class='dark-bg white'&gt;Text&lt;/span&gt; Format. <br> Rotate Text: &lt;span class='ultrabold rotate'&gt;creative, unique, minimal&lt;/span&gt;. <br> Colored Text: Colored &lt;span class='colored'&gt;Text&lt;/span&gt; Format.",
			), 
			
			
			
	
        )
) );

//subtitle
class WPBakeryShortCode_WR_VC_Header_One_Subtitle extends WPBakeryShortCode {}
vc_map( array(
        "name" => "Sub Title", "cinestar",
        "base" => "wr_vc_header_one_subtitle",
        "content_element" => true,
		"icon" => "icon-wpb-qode_client",
        "as_child" => array('only' => 'wr_vc_head_simple, wr_vc_head_four, wr_vc_head_five, wr_vc_head_six, wr_vc_head_seven'), // Use only|except attributes to limit parent (separate multiple values with comma)
        "params" => array(
				
			
			array(
				"type" => "textarea_html",
				"holder" => "div",
				"class" => "",
				"heading" => "Title",
				"param_name" => "content",
				"value" => "",
				"description" => "Bold Text: Bold &lt;span class='bold'&gt;Text&lt;/span&gt; Format. <br> Italic Text: Italic &lt;span class='italic'&gt;Text&lt;/span&gt; Format.",
			), 
			
			
			
	
        )
) );


//header 2
class WPBakeryShortCode_Wr_Vc_Head_Two  extends WPBakeryShortCodesContainer {}
//Register "container" content element. It will hold all your inner (child) content elements
vc_map( array(
        "name" => "Header Style 2", "cinestar",
        "base" => "wr_vc_head_two",
		"as_parent" => array('only' => 'wr_vc_header_two_title, wr_vc_header_two_subtitle,  wr_vc_profiles, wr_vc_single_profile'),
		"content_element" => true,
		"category" => 'Page Header',
		"icon" => "icon-wpb-qode_clients",
        "show_settings_on_create" => true,
        "params" => array(
			
			
			

        ),
        "js_view" => 'VcColumnView'
) );



//title
class WPBakeryShortCode_WR_VC_Header_Two_Title extends WPBakeryShortCode {}
vc_map( array(
        "name" => "Title", "cinestar",
        "base" => "wr_vc_header_two_title",
        "content_element" => true,
		"icon" => "icon-wpb-qode_client",
        "as_child" => array('only' => 'wr_vc_head_two'), // Use only|except attributes to limit parent (separate multiple values with comma)
        "params" => array(
				
			
			array(
				"type" => "textarea_html",
				"holder" => "div",
				"class" => "",
				"heading" => "Title",
				"param_name" => "content",
				"value" => "",
				"description" => "Markup Text: Markup &lt;span class='dark-bg white'&gt;Text&lt;/span&gt; Format. <br> Rotate Text: &lt;span class='ultrabold rotate'&gt;creative, unique, minimal&lt;/span&gt;.",
			), 
			
			
			
	
        )
) );

//subtitle
class WPBakeryShortCode_WR_VC_Header_Two_Subtitle extends WPBakeryShortCode {}
vc_map( array(
        "name" => "Sub Title", "cinestar",
        "base" => "wr_vc_header_two_subtitle",
        "content_element" => true,
		"icon" => "icon-wpb-qode_client",
        "as_child" => array('only' => 'wr_vc_head_two, wr_vc_head_eight  '), // Use only|except attributes to limit parent (separate multiple values with comma)
        "params" => array(
				
			
			array(
				"type" => "textarea_html",
				"holder" => "div",
				"class" => "",
				"heading" => "Title",
				"param_name" => "content",
				"value" => "",
				"description" => "Bold Text: Bold &lt;span class='bold'&gt;Text&lt;/span&gt; Format",
			), 
			
			
			
	
        )
) );

//header 3
class WPBakeryShortCode_Wr_Vc_Head_Three  extends WPBakeryShortCodesContainer {}
//Register "container" content element. It will hold all your inner (child) content elements
vc_map( array(
        "name" => "Header Style 3", "cinestar",
        "base" => "wr_vc_head_three",
		"as_parent" => array('only' => 'wr_vc_header_three_title, wr_vc_header_three_subtitle,  wr_vc_profiles, wr_vc_single_profile'),
		"content_element" => true,
		"category" => 'Page Header',
		"icon" => "icon-wpb-qode_clients",
        "show_settings_on_create" => true,
        "params" => array(
			
			
			

        ),
        "js_view" => 'VcColumnView'
) );

//title
class WPBakeryShortCode_WR_VC_Header_Three_Title extends WPBakeryShortCode {}
vc_map( array(
        "name" => "Title", "cinestar",
        "base" => "wr_vc_header_three_title",
        "content_element" => true,
		"icon" => "icon-wpb-qode_client",
        "as_child" => array('only' => 'wr_vc_head_three'), // Use only|except attributes to limit parent (separate multiple values with comma)
        "params" => array(
				
			
			array(
				"type" => "textarea_html",
				"holder" => "div",
				"class" => "",
				"heading" => "Title",
				"param_name" => "content",
				"value" => "",
				"description" => "Bold Text: Bold &lt;span class='bold'&gt;Text&lt;/span&gt; Format. <br> Rotate Text: &lt;span class='ultrabold rotate'&gt;creative, unique, minimal&lt;/span&gt;.",
			), 
			
			
			
	
        )
) );

//subtitle
class WPBakeryShortCode_WR_VC_Header_Three_Subtitle extends WPBakeryShortCode {}
vc_map( array(
        "name" => "Sub Title", "cinestar",
        "base" => "wr_vc_header_three_subtitle",
        "content_element" => true,
		"icon" => "icon-wpb-qode_client",
        "as_child" => array('only' => 'wr_vc_head_three'), // Use only|except attributes to limit parent (separate multiple values with comma)
        "params" => array(
				
			
			array(
				"type" => "textarea_html",
				"holder" => "div",
				"class" => "",
				"heading" => "Title",
				"param_name" => "content",
				"value" => "",
				"description" => "Bold Text: Bold &lt;span class='bold'&gt;Text&lt;/span&gt; Format",
			), 
			
			
			
	
        )
) );

//header 4
class WPBakeryShortCode_Wr_Vc_Head_Four  extends WPBakeryShortCodesContainer {}
//Register "container" content element. It will hold all your inner (child) content elements
vc_map( array(
        "name" => "Header Style 4", "cinestar",
        "base" => "wr_vc_head_four",
		"as_parent" => array('only' => 'wr_vc_header_four_title, wr_vc_header_one_subtitle,  wr_vc_profiles, wr_vc_single_profile'),
		"content_element" => true,
		"category" => 'Page Header',
		"icon" => "icon-wpb-qode_clients",
        "show_settings_on_create" => true,
        "params" => array(
			
			
			

        ),
        "js_view" => 'VcColumnView'
) );

//title
class WPBakeryShortCode_WR_VC_Header_Four_Title extends WPBakeryShortCode {}
vc_map( array(
        "name" => "Title", "cinestar",
        "base" => "wr_vc_header_four_title",
        "content_element" => true,
		"icon" => "icon-wpb-qode_client",
        "as_child" => array('only' => 'wr_vc_head_four'), // Use only|except attributes to limit parent (separate multiple values with comma)
        "params" => array(
				
			
			array(
				"type" => "textarea_html",
				"holder" => "div",
				"class" => "",
				"heading" => "Title",
				"param_name" => "content",
				"value" => "",
				"description" => "Bold Text: Bold &lt;span class='bold'&gt;Text&lt;/span&gt; Format. <br> Rotate Text: &lt;span class='ultrabold rotate'&gt;creative, unique, minimal&lt;/span&gt;.",
			), 
			
			
			
	
        )
) );

//header 5
class WPBakeryShortCode_Wr_Vc_Head_Five  extends WPBakeryShortCodesContainer {}
//Register "container" content element. It will hold all your inner (child) content elements
vc_map( array(
        "name" => "Header Style 5", "cinestar",
        "base" => "wr_vc_head_five",
		"as_parent" => array('only' => 'wr_vc_header_five_title, wr_vc_header_one_subtitle,  wr_vc_profiles, wr_vc_single_profile'),
		"content_element" => true,
		"category" => 'Page Header',
		"icon" => "icon-wpb-qode_clients",
        "show_settings_on_create" => true,
        "params" => array(
			
			
			

        ),
        "js_view" => 'VcColumnView'
) );

//title
class WPBakeryShortCode_WR_VC_Header_Five_Title extends WPBakeryShortCode {}
vc_map( array(
        "name" => "Title", "cinestar",
        "base" => "wr_vc_header_five_title",
        "content_element" => true,
		"icon" => "icon-wpb-qode_client",
        "as_child" => array('only' => 'wr_vc_head_five'), // Use only|except attributes to limit parent (separate multiple values with comma)
        "params" => array(
				
			
			array(
				"type" => "textarea_html",
				"holder" => "div",
				"class" => "",
				"heading" => "Title",
				"param_name" => "content",
				"value" => "",
				"description" => "Bold Text: Bold &lt;span class='bold'&gt;Text&lt;/span&gt; Format. <br> Rotate Text: &lt;span class='ultrabold rotate'&gt;creative, unique, minimal&lt;/span&gt;.",
			), 
			
			
			
	
        )
) );

//header 6
class WPBakeryShortCode_Wr_Vc_Head_Six  extends WPBakeryShortCodesContainer {}
//Register "container" content element. It will hold all your inner (child) content elements
vc_map( array(
        "name" => "Header Style 6", "cinestar",
        "base" => "wr_vc_head_six",
		"as_parent" => array('only' => 'wr_vc_header_six_title, wr_vc_header_one_subtitle,  wr_vc_profiles, wr_vc_single_profile'),
		"content_element" => true,
		"category" => 'Page Header',
		"icon" => "icon-wpb-qode_clients",
        "show_settings_on_create" => true,
        "params" => array(
			
			
			

        ),
        "js_view" => 'VcColumnView'
) );

//title
class WPBakeryShortCode_WR_VC_Header_Six_Title extends WPBakeryShortCode {}
vc_map( array(
        "name" => "Title", "cinestar",
        "base" => "wr_vc_header_six_title",
        "content_element" => true,
		"icon" => "icon-wpb-qode_client",
        "as_child" => array('only' => 'wr_vc_head_six'), // Use only|except attributes to limit parent (separate multiple values with comma)
        "params" => array(
				
			
			array(
				"type" => "textarea_html",
				"holder" => "div",
				"class" => "",
				"heading" => "Title",
				"param_name" => "content",
				"value" => "",
				"description" => "Bold Text: Bold &lt;span class='bold'&gt;Text&lt;/span&gt; Format. <br> Rotate Text: &lt;span class='ultrabold rotate'&gt;creative, unique, minimal&lt;/span&gt;.",
			), 
		
	
        )
) );

//header 7
class WPBakeryShortCode_Wr_Vc_Head_Seven  extends WPBakeryShortCodesContainer {}
//Register "container" content element. It will hold all your inner (child) content elements
vc_map( array(
        "name" => "Header Style 7", "cinestar",
        "base" => "wr_vc_head_seven",
		"as_parent" => array('only' => 'wr_vc_header_seven_title, wr_vc_header_one_subtitle,  wr_vc_profiles, wr_vc_single_profile'),
		"content_element" => true,
		"category" => 'Page Header',
		"icon" => "icon-wpb-qode_clients",
        "show_settings_on_create" => true,
        "params" => array(
			
			
			

        ),
        "js_view" => 'VcColumnView'
) );

//title
class WPBakeryShortCode_WR_VC_Header_Seven_Title extends WPBakeryShortCode {}
vc_map( array(
        "name" => "Title", "cinestar",
        "base" => "wr_vc_header_seven_title",
        "content_element" => true,
		"icon" => "icon-wpb-qode_client",
        "as_child" => array('only' => 'wr_vc_head_seven'), // Use only|except attributes to limit parent (separate multiple values with comma)
        "params" => array(
				
			
			array(
				"type" => "textarea_html",
				"holder" => "div",
				"class" => "",
				"heading" => "Title",
				"param_name" => "content",
				"value" => "",
				"description" => "Bold Text: Bold &lt;span class='bold'&gt;Text&lt;/span&gt; Format. <br> Rotate Text: &lt;span class='ultrabold rotate'&gt;creative, unique, minimal&lt;/span&gt;.",
			), 
		
	
        )
) );

//header 8
class WPBakeryShortCode_Wr_Vc_Head_Eight  extends WPBakeryShortCodesContainer {}
//Register "container" content element. It will hold all your inner (child) content elements
vc_map( array(
        "name" => "Header Style 8", "cinestar",
        "base" => "wr_vc_head_eight",
		"as_parent" => array('only' => 'wr_vc_header_eight_title, wr_vc_header_two_subtitle,  wr_vc_profiles, wr_vc_single_profile'),
		"content_element" => true,
		"category" => 'Page Header',
		"icon" => "icon-wpb-qode_clients",
        "show_settings_on_create" => true,
        "params" => array(
			
			
			

        ),
        "js_view" => 'VcColumnView'
) );

//title
class WPBakeryShortCode_WR_VC_Header_Eight_Title extends WPBakeryShortCode {}
vc_map( array(
        "name" => "Title", "cinestar",
        "base" => "wr_vc_header_eight_title",
        "content_element" => true,
		"icon" => "icon-wpb-qode_client",
        "as_child" => array('only' => 'wr_vc_head_eight'), // Use only|except attributes to limit parent (separate multiple values with comma)
        "params" => array(
				
			
			array(
				"type" => "textarea_html",
				"holder" => "div",
				"class" => "",
				"heading" => "Title",
				"param_name" => "content",
				"value" => "",
				"description" => "Bold Text: Bold &lt;span class='bold'&gt;Text&lt;/span&gt; Format. <br> Rotate Text: &lt;span class='rotate'&gt;creative, unique, minimal&lt;/span&gt;.",
			), 
		
	
        )
) );


//header 9
class WPBakeryShortCode_Wr_Vc_Head_Nine  extends WPBakeryShortCodesContainer {}
//Register "container" content element. It will hold all your inner (child) content elements
vc_map( array(
        "name" => "Header Style 9", "cinestar",
        "base" => "wr_vc_head_nine",
		"as_parent" => array('only' => 'wr_vc_header_nine_title, wr_vc_header_nine_subtitle,  wr_vc_profiles, wr_vc_single_profile'),
		"content_element" => true,
		"category" => 'Page Header',
		"icon" => "icon-wpb-qode_clients",
        "show_settings_on_create" => true,
        "params" => array(
			
			
			

        ),
        "js_view" => 'VcColumnView'
) );

//title
class WPBakeryShortCode_WR_VC_Header_Nine_Title extends WPBakeryShortCode {}
vc_map( array(
        "name" => "Title", "cinestar",
        "base" => "wr_vc_header_nine_title",
        "content_element" => true,
		"icon" => "icon-wpb-qode_client",
        "as_child" => array('only' => 'wr_vc_head_nine'), // Use only|except attributes to limit parent (separate multiple values with comma)
        "params" => array(
				
			
			array(
				"type" => "textarea_html",
				"holder" => "div",
				"class" => "",
				"heading" => "Title",
				"param_name" => "content",
				"value" => "",
				"description" => "Separat word with , e.x: Creative, Idea, Amio for run slider . ",
			), 
			
			
			
	
        )
) );

//subtitle
class WPBakeryShortCode_WR_VC_Header_Nine_Subtitle extends WPBakeryShortCode {}
vc_map( array(
        "name" => "Sub Title", "cinestar",
        "base" => "wr_vc_header_nine_subtitle",
        "content_element" => true,
		"icon" => "icon-wpb-qode_client",
        "as_child" => array('only' => 'wr_vc_head_three'), // Use only|except attributes to limit parent (separate multiple values with comma)
        "params" => array(
				
			
			array(
				"type" => "textarea_html",
				"holder" => "div",
				"class" => "",
				"heading" => "Title",
				"param_name" => "content",
				"value" => "",
				"description" => "Bold Text: Bold &lt;span class='bold'&gt;Text&lt;/span&gt; Format",
			), 
			
			
			
	
        )
) );

//  Feature Icon
vc_map( array(
		"name" => "Amio Feature",
		"base" => "amio_service",
		"category" => 'bY Amio',
		"icon" => "",
		"allowed_container_element" => 'vc_row',
		"params" => array(
			
			
			
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => "Title",
				"param_name" => "title",
				"value" => "",
				"description" => ""
			),
			
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => "Icon Class",
				"param_name" => "class",
				"value" => "",
				"description" => ""
			),
			
			
			array(
				"type" => "textarea_html",
				"holder" => "div",
				"class" => "",
				"heading" => "Content",
				"param_name" => "content",
				"value" => "",
				"description" => ""
			),
			
		)
) );


// wr slider
class WPBakeryShortCode_WR_VC_Sliders  extends WPBakeryShortCodesContainer {}
//Register "container" content element. It will hold all your inner (child) content elements
vc_map( array(
        "name" => esc_attr__('Amio Image Slider', 'amio'),
        "base" => "wr_vc_sliders",
        "as_parent" => array('only' => 'wr_vc_slider'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
        "content_element" => true,
		"category" => esc_attr__('bY Amio', 'amio'),
		"icon" => "icon-wpb-layerslider",
        "show_settings_on_create" => true,
        "params" => array(
		
            
        ),
        "js_view" => 'VcColumnView'
) );

class WPBakeryShortCode_WR_VC_Slider extends WPBakeryShortCode {}
vc_map( array(
        "name" => esc_attr__('Amio Slide Item', 'amio'),
        "base" => "wr_vc_slider",
        "content_element" => true,
		"icon" => "icon-wpb-qode_client",
        "as_child" => array('only' => 'wr_vc_sliders'), // Use only|except attributes to limit parent (separate multiple values with comma)
        "params" => array(
				
			
			array(
				"type" => "attach_image",
				"holder" => "div",
				"class" => "",
				"heading" => esc_attr__('Image', 'amio'),
				"param_name" => "image",
				"description" => ""
			),
			
        )
) );


// Pricing Block
class WPBakeryShortCode_WR_VC_Pricing  extends WPBakeryShortCodesContainer {}
//Register "container" content element. It will hold all your inner (child) content elements
vc_map( array(
        "name" => "Amio Pricing", "amio",
        "base" => "wr_vc_pricing",
        "as_parent" => array('only' => 'wr_vc_pricing_list'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
        "content_element" => true,
		"category" => 'bY Amio',
		"icon" => "icon-wpb-qode_clients",
        "show_settings_on_create" => true,
        "params" => array(
						
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => "Title",
				"param_name" => "title",
				"value" => ""
			),   
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => "Amount",
				"param_name" => "price",
				"value" => "",
			),		
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => "Icon Class",
				"param_name" => "period",
				"value" => "",
			),		
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => "Button Text",
				"param_name" => "button_text",
				"value" => "",
			),
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => "Button Link URL",
				"param_name" => "button_link",
				"value" => "",
			),			
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => "Link Target",
				"param_name" => "link_target",
				"value" => array(
					"Self" => "_self",
					"Blank" => "_blank",
					"Parent" => "_parent",	
					"Top" => "_top"	
				),
				"description" => "",
			),				
			array(
				"type" => "dropdown",
				"holder" => "div",
				"class" => "",
				"heading" => "Recommended",
				"param_name" => "active",
				"value" => array(
					"Disable" => "deactive",
					"Enable" => "table-special",
				),
				"description" => "",
			),						
            
        ),
        "js_view" => 'VcColumnView'
) );

class WPBakeryShortCode_WR_VC_Pricing_List extends WPBakeryShortCode {}
vc_map( array(
        "name" => "Amio Pricing Table List", "amio",
        "base" => "wr_vc_pricing_list",
        "content_element" => true,
		"icon" => "icon-wpb-qode_client",
        "as_child" => array('only' => 'wr_vc_pricing'), // Use only|except attributes to limit parent (separate multiple values with comma)
        "params" => array(
			array(
				"type" => "textarea_html",
				"holder" => "div",
				"class" => "",
				"heading" => "Pricing Table List",
				"param_name" => "content",
				"value" => ""
			),						
            
        )
) );



?>