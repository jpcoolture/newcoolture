<?php

$args = array(
    	'title'=>'',
    	'contf'=>'',
    	'wrtext'=>'',
    	'buttontext'=>'',
    	'enablemap'=>'',
    	'image'=>'',
);

$html = "";

extract(shortcode_atts($args, $atts));

$html .= '<div class="header-img multi-img col-md-12 t-left">';
$html .= do_shortcode($content);
$html .= '</div>';

echo $html;