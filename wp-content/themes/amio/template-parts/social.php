<?php defined('ABSPATH') or die("No script kiddies please!");?>
<?php $cinestar_options = get_option('cinestar_'); ?>
<?php
global $cinestar_link_target;
if($cinestar_options['wr-social-target'] == 'self') {
		$cinestar_link_target = 'target="_self"';
}
else	if($cinestar_options['wr-social-target'] == 'blank') {
		$cinestar_link_target = 'target="_blank"';
}
?>
<?php if(!empty($cinestar_options['facebook1'])):?>
<a href="<?php echo esc_url($cinestar_options['facebook1']);?>" <?php echo esc_attr($cinestar_link_target);?>><i class="pe-so-facebook"></i></a>
<?php endif;?>
<?php if(!empty($cinestar_options['twitter1'])):?>
<a href="<?php echo esc_url($cinestar_options['twitter1']);?>" <?php echo esc_attr($cinestar_link_target);?>><i class="pe-so-twitter"></i></a>
<?php endif;?>
<?php if(!empty($cinestar_options['gplus1'])):?>
<a href="<?php echo esc_url($cinestar_options['gplus1']);?>" <?php echo esc_attr($cinestar_link_target);?>><i class="pe-so-google-plus"></i></a>
<?php endif;?>
<?php if(!empty($cinestar_options['behance1'])):?>
<a href="<?php echo esc_url($cinestar_options['behance1']);?>" <?php echo esc_attr($cinestar_link_target);?>><i class="pe-so-behance"></i></a>
<?php endif;?>
<?php if(!empty($cinestar_options['pinterest1'])):?>
<a href="<?php echo esc_url($cinestar_options['pinterest1']);?>" <?php echo esc_attr($cinestar_link_target);?>><i class="pe-so-pinterest"></i></a>
<?php endif;?>
<?php if(!empty($cinestar_options['linkedin1'])):?>
<a href="<?php echo esc_url($cinestar_options['linkedin1']);?>" <?php echo esc_attr($cinestar_link_target);?>><i class="pe-so-linkedin"></i></a>
<?php endif;?>
<?php if(!empty($cinestar_options['instagram1'])):?>
<a href="<?php echo esc_url($cinestar_options['instagram1']);?>" <?php echo esc_attr($cinestar_link_target);?>><i class="pe-so-instagram"></i></a>
<?php endif;?>
<?php if(!empty($cinestar_options['youtube1'])):?>
<a href="<?php echo esc_url($cinestar_options['youtube1']);?>" <?php echo esc_attr($cinestar_link_target);?>><i class="pe-so-youtube-1"></i></a>
<?php endif;?>
<?php if(!empty($cinestar_options['git1'])):?>
<a href="<?php echo esc_url($cinestar_options['git1']);?>" <?php echo esc_attr($cinestar_link_target);?>><i class="pe-so-git"></i></a>
<?php endif;?>
<?php if(!empty($cinestar_options['vimeo1'])):?>
<a href="<?php echo esc_url($cinestar_options['vimeo1']);?>" <?php echo esc_attr($cinestar_link_target);?>><i class="pe-so-vimeo"></i></a>
<?php endif;?>
<?php if(!empty($cinestar_options['tripadvisor1'])):?>
<a href="<?php echo esc_url($cinestar_options['tripadvisor1']);?>" <?php echo esc_attr($cinestar_link_target);?>><i class="pe-so-tripadvisor"></i></a>
<?php endif;?>
<?php if(!empty($cinestar_options['xing1'])):?>
<a href="<?php echo esc_url($cinestar_options['xing1']);?>" <?php echo esc_attr($cinestar_link_target);?>><i class="pe-so-xing"></i></a>
<?php endif;?>
<?php if(!empty($cinestar_options['dribbble1'])):?>
<a href="<?php echo esc_url($cinestar_options['dribbble1']);?>" <?php echo esc_attr($cinestar_link_target);?>><i class="pe-so-dribbble"></i></a>
<?php endif;?>
<?php if(!empty($cinestar_options['soundcloud1'])):?>
<a href="<?php echo esc_url($cinestar_options['soundcloud1']);?>" <?php echo esc_attr($cinestar_link_target);?>><i class="pe-so-soundcloud"></i></a>
<?php endif;?>