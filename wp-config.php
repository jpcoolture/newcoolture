<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'newcoolture');

/** MySQL database username */
define('DB_USER', 'newcoolture');

/** MySQL database password */
define('DB_PASSWORD', 'en3AuBWxVhbtLzCQ');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ']}I_5;k*XT;eAvrpX_{Uow^jOOU)-IJJfDFaW?C(|lS/d5$NNUR&L^t{qJ#kve;Q');
define('SECURE_AUTH_KEY',  'P, k%bLd}L7&5T#!L{V 2ri,gs%*aKWHT=vhD.SkZ0BT;%?Vb7g1zcK.N^; VSp0');
define('LOGGED_IN_KEY',    'E=~s!_RqAXu`n+;0[uz!(bf*.ZWifDbxUpf;U(Y~52zuE~4`<2*K:,]9J9ddHqA%');
define('NONCE_KEY',        'O}Q@(7^uGD?*;$YCN[|x;RPPx;>7=y<jX(!WcP{a? VgpGB9aWA,gajsk_NVu<~m');
define('AUTH_SALT',        '<W8Ea^C|VDk{M~lV200/i./;PRnS+Ttt!QabL;oyV`T0J7Kg$+3EMh;[f{ksWv7?');
define('SECURE_AUTH_SALT', 'X!q)7iX:^y>i#3~YoE^#ZugutA}?M/+sq8ZHI1-7i^cqK;{5q_EJ1}JI1-D<k7nb');
define('LOGGED_IN_SALT',   '1ds|HNydb7f9T{Q>rg4z?+ag;W1V&;xXqcxKM+a]vT]xdv=r;6.dZUwS&3sU~Aw,');
define('NONCE_SALT',       '_JGuuk2|nDcZ7{+Lpi,s.:0uQ}Ey9.ia,P94_3d9bUuL,M:T2Ip<7{w;m`+]@WeQ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
