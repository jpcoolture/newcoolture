<!-- Header -->
				<div id="header">
				
					<!-- Section Content -->
					<div class="section-content">
				
						
						<!-- Heading -->
						<div class="big-text" data-uk-scrollspy="{cls:'uk-animation-fade', delay:20}">
						
							<!-- Title -->
							<h2 class="t-left f-large ultrabold uppercase"><?php the_title();?></h2>
							
							<div class="separator-small"></div>
							
							<!-- Description -->
							<h3 class="t-left f-medium normal uppercase"><?php echo esc_attr(get_post_meta($post->ID,'rnr_pagesubtt',true));?></h3>
							
						</div>
						<!-- End Heading -->
						
						
						
					</div>
					<!-- End Section Content -->
					
				</div>
				<!-- End Header -->
<div id="about" class="text-left">
<!-- Section Content -->
<div class="section-content">
<?php while ( have_posts() ) : the_post(); ?>
<?php the_content();?>
<?php endwhile; ?>
<?php if ( comments_open() || get_comments_number() ) {comments_template();} ?>
</div>
</div>