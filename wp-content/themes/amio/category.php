<?php $amio_options = get_option('amio'); ?> 
<?php
get_header();
 ?>
<?php if (class_exists('WooCommerce')) { ?>
<ul class="nav navbar-nav f-small woo-nab-ico normal raleway"><li><a href="<?php echo $woocommerce->cart->get_cart_url(); ?>"><i class="ion-bag f-normal"></i></a></li></ul>
<?php } ?>
<?php get_template_part('template-parts/after-menu-section');?>
 <!-- Header -->
				<div id="header">
				
					<!-- Section Content -->
					<div class="section-content">
				
						
						<!-- Heading -->
						<div class="big-text" data-uk-scrollspy="{cls:'uk-animation-fade', delay:20}">
						
							<!-- Title -->
							<h2 class="t-left f-large ultrabold uppercase">
							<?php esc_attr_e('Category:','amio');?>
							
							</h2>
							
							<!-- Description -->
							<h3 class="t-left f-medium normal uppercase"><?php single_cat_title( '', true ); ?></h3>
							
						</div>
						<!-- End Heading -->
						
						
						
					</div>
					<!-- End Section Content -->
					
				</div>
				<!-- End Header -->
 <!-- About -->
<div id="blog">
<!-- Section Content -->
	<div class="section-content">
	
	    <?php if ($amio_options['blog-sidebar']=="st1") {?>
        <?php get_template_part('template-parts/blog/index-left');?>
		<?php } elseif ($amio_options['blog-sidebar']=="st3") {?>
		<?php get_template_part('template-parts/blog/index-full');?>
		<?php } elseif ($amio_options['blog-sidebar']=="st4") {?>
		<?php get_template_part('template-parts/blog/index-four');?>
		<?php } else { ?>
		<?php get_template_part('template-parts/blog/index-right');?>
		<?php } ?>
	    
	</div>		
</div>		
<?php get_footer(); ?>	