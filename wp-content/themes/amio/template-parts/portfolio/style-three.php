<?php $amio_options = get_option('amio'); ?> 
<!-- Row -->
						<div class="row">
						
							<!-- Column -->
							<div class="col-md-12">
							<!-- Images -->
								<ul class="rslides about-slide">
								<?php $amio_images = rwmb_meta( 'rnr_portfolio-image','type=image&size=' );
                                    foreach ( $amio_images as $amio_image ){
                                        echo "<li><img src='{$amio_image['url']}' alt=''></li>
							   ";
								 };?>
									
		 
								</ul>
								<!-- End Images -->
								
							</div>
							<!-- End Column -->
							
						</div>
						<!-- End Row -->
		<!-- Row -->
		<div class="row">
			<!-- Column -->
			<div class="col-md-12 t-left brief">
				<?php if(have_posts()) : while ( have_posts() ) : the_post();?>
				<?php the_content();?>
				<?php endwhile;  endif; wp_reset_postdata(); ?>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->