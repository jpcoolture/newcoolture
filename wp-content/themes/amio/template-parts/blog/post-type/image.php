<?php if (has_post_thumbnail( $post->ID ) ):
$amio_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'amio_blog_image' );?>

	<a href="<?php the_permalink();?>">
		<img src="<?php echo esc_url($amio_image[0]);?>" alt="<?php the_title();?>">
	</a>

<?php endif;?>
