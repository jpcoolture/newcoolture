<?php defined('ABSPATH') or die("No script kiddies please!");?>
<?php $amio_options = get_option('amio'); ?> 
<?php
get_header();
 ?>
<?php if (class_exists('WooCommerce')) { ?>
<ul class="nav navbar-nav f-small woo-nab-ico normal raleway"><li><a href="<?php echo $woocommerce->cart->get_cart_url(); ?>"><i class="ion-bag f-normal"></i></a></li></ul>
<?php } ?>
<?php get_template_part('template-parts/after-menu-section');?>
<!-- Header -->
				<div id="header">
				
					<!-- Section Content -->
					<div class="section-content">
				
						
						<!-- Heading -->
						<div class="big-text" data-uk-scrollspy="{cls:'uk-animation-fade', delay:20}">
						
							<!-- Title -->
							<h2 class="t-left f-large ultrabold uppercase"><?php the_title();?></h2>
							
							<!-- Description -->
							<h3 class="t-left f-medium normal uppercase"><?php echo esc_attr(get_post_meta($post->ID,'rnr_bl_sub_title',true));?></h3>
							
						</div>
						<!-- End Heading -->
						
						
						
					</div>
					<!-- End Section Content -->
					
				</div>
				<!-- End Header -->
<!-- Blog -->
				<div id="blog-post">
					
					
					
					<!-- Section Content -->
					<div class="section-content">
					
					<?php get_template_part('template-parts/blog-page-header');?>
					<?php if ($amio_options['blogdetst']=="st1") {?>
					<?php get_template_part('template-parts/blog/single/left');?>
					<?php } else if ($amio_options['blogdetst']=="st3") { ?>
					<?php get_template_part('template-parts/blog/single/full');?>
					<?php } else { ?>
					<?php get_template_part('template-parts/blog/single/right');?>
					<?php } ;?>	
						
					</div>
					<!-- End Section Content -->
					
				</div>
				<!-- End Blog -->
				
				
				
				
				
				

<?php get_footer(); ?> 