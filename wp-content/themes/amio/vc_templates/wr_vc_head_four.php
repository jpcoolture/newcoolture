<?php

$args = array(
		'class'=>'',
		'title'=>'',
		'subtitle'=>'',
		
		
		
);

extract(shortcode_atts($args, $atts));

$html = '';
$amio_dot = "'";
$html .= '<div id="header">';
$html .= '<div class="section-content">';
$html .= '<div class="big-text-full" data-uk-scrollspy="{cls:'.$amio_dot.'uk-animation-fade'.$amio_dot.', delay:20}">';
$html .= do_shortcode($content);
$html .= '</div>';
$html .= '</div>';
$html .= '</div>';

    


echo $html;