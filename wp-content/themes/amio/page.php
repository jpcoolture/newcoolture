<?php $amio_options = get_option('amio'); ?> 
<?php
get_header();
 ?>
<?php if (class_exists('WooCommerce')) { ?>
<ul class="nav navbar-nav f-small woo-nab-ico normal raleway"><li><a href="<?php echo $woocommerce->cart->get_cart_url(); ?>"><i class="ion-bag f-normal"></i></a></li></ul>
<?php } ?>
<?php get_template_part('template-parts/after-menu-section');?>
		 
		 <!--content area-->
		 <?php if(get_post_meta($post->ID,'rnr_wr-pagetype',true)=='st2'){ ?> 
         <?php get_template_part('template-parts/pages/amio-element');?>
		 <?php }
		 
		 else  { ?>
		 <?php get_template_part('template-parts/pages/default');?>
		 <?php }?>
		 <!--content area-->
 
<?php get_footer(); ?>	
