<?php $amio_options = get_option('amio'); ?> 
<!-- Row -->
						<div class="row">
						
							<!-- Column -->
							<div class="col-md-12">
							<div data-uk-grid="{controls: '#gallary'}">
							<?php $amio_images = rwmb_meta( 'rnr_portfolio-image','type=image&size=' );
                                    foreach ( $amio_images as $amio_image ){
                                        echo "<div class='work uk-width-small-1-1 uk-width-medium-1-3' data-uk-filter='filter-web'>
								
								
										<!-- Gallary Image -->
										<img src='{$amio_image['url']}' alt='' />
										<!-- End Work Image -->
										
										
										<!-- Hover Lightbox -->
										<a href='{$amio_image['url']}' data-lightbox='gallary' title=''>
										
											<!-- Hover -->
											<div class='hover'>
											
													<!-- Title -->
													
													
											</div>
											<!-- End Hover -->
										
										</a>
										<!-- End Hover Lightbox -->
										
									</div>
							   ";
								 };?>
							
							</div>
								
							</div>
							<!-- End Column -->
							
						</div>
						<!-- End Row -->
		<!-- Row -->
		<div class="row">
			<!-- Column -->
			<div class="col-md-12 t-left brief">
				<?php if(have_posts()) : while ( have_posts() ) : the_post();?>
				<?php the_content();?>
				<?php endwhile;  endif; wp_reset_postdata(); ?>
			</div>
			<!-- End Column -->
		</div>
		<!-- End Row -->