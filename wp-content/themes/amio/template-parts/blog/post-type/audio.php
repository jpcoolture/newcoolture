<?php if (get_post_meta($post->ID,'rnr_bl-audio',true)!=''):?>
<!-- Begin SoundCloud embed -->
<div class="soundcloud-embed">
	<iframe style="width:100%; height:166px" src="<?php echo esc_url(get_post_meta($post->ID,'rnr_bl-audio',true));?>"></iframe>
</div>
<!-- End SoundCloud embed -->
<br>
<?php endif;?>
										