<?php

$args = array(
    	'page_name'=>'',
		'boxwidth'=>'',
		'boxheight'=>'',
		'title'=>'',
		'boxtitle'=>'',
);

$html = "";

extract(shortcode_atts($args, $atts));

$html .= '<ul class="rslides about-slide">';
$html .= do_shortcode($content);         
$html .= '</ul>';

echo $html;