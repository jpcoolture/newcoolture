<?php $amio_options = get_option('amio'); ?> 
<?php
/*Template Name:404 Page*/
get_header();

 ?>
<?php if (class_exists('WooCommerce')) { ?>
<ul class="nav navbar-nav f-small normal woo-nab-ico raleway"><li><a href="<?php echo $woocommerce->cart->get_cart_url(); ?>"><i class="ion-bag f-normal"></i></a></li></ul>
<?php } ?>
<?php get_template_part('template-parts/after-menu-section');?>
<!-- Header -->
				<div id="header">
				
					<!-- Section Content -->
					<div class="section-content">
				
						
						<!-- Heading -->
						<div class="big-text" data-uk-scrollspy="{cls:'uk-animation-fade', delay:20}">
						
						
							<!-- Icon -->
							<i class="icon ion-map col-md-12 t-left"></i>
							
							<!-- Title -->
							<h2 class="t-left f-large ultrabold uppercase"><?php esc_attr_e('Oops! It looks like you are lost.','amio');?></h2>
							
							<!-- Description -->
							<h3 class="t-left f-medium normal uppercase"><?php esc_attr_e('The page you are looking for does not exist. ','amio');?><a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="bold"><?php esc_attr_e('Click here','amio');?></a> <?php esc_attr_e('to go back to the home page.','amio');?></h3>
							
						</div>
						<!-- End Heading -->
						
						
						
					</div>
					<!-- End Section Content -->
					
				</div>
				<!-- End Header -->
<?php get_footer(); ?>	
