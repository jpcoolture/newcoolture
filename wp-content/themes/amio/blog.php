<?php $amio_options = get_option('amio'); ?> 
<?php
/*Template Name:Blog Page Template*/
 get_header();
 
 
 ?>
<?php if (class_exists('WooCommerce')) { ?>
<ul class="nav navbar-nav f-small normal woo-nab-ico raleway"><li><a href="<?php echo $woocommerce->cart->get_cart_url(); ?>"><i class="ion-bag f-normal"></i></a></li></ul>
<?php } ?>
<?php get_template_part('template-parts/after-menu-section');?>
 <!-- Header -->
				<div id="header">
				
					<!-- Section Content -->
					<div class="section-content">
				
						
						<!-- Heading -->
						<div class="big-text" data-uk-scrollspy="{cls:'uk-animation-fade', delay:20}">
						
							<!-- Title -->
							<h2 class="t-left f-large ultrabold uppercase"><?php the_title();?></h2>
							
							<!-- Description -->
							<h3 class="t-left f-medium normal uppercase"><?php echo esc_attr(get_post_meta($post->ID,'rnr_pagesubtt',true));?></h3>
							
						</div>
						<!-- End Heading -->
						
						
						
					</div>
					<!-- End Section Content -->
					
				</div>
				<!-- End Header -->
 <!-- About -->
<div id="blog">
<!-- Section Content -->
	<div class="section-content">
		<?php if(get_post_meta($post->ID,'rnr_wrblog-pagetype',true)=='st1'){ ?> 
        <?php get_template_part('template-parts/blog/default');?>
		 <?php }
		else if(get_post_meta($post->ID,'rnr_wrblog-pagetype',true)=='st2') { ?>
         <?php get_template_part('template-parts/blog/blog-four');?>
		<?php }
		else if(get_post_meta($post->ID,'rnr_wrblog-pagetype',true)=='st3') { ?>
         <?php get_template_part('template-parts/blog/blog-left');?>
		<?php }
		else if(get_post_meta($post->ID,'rnr_wrblog-pagetype',true)=='st4') { ?>
         <?php get_template_part('template-parts/blog/blog-right');?>
		<?php }
		else  { ?>
		<?php get_template_part('template-parts/blog/default');?>
		<?php }?>
	</div>		
</div>		
<?php get_footer(); ?>	