<?php

$args = array(
    	'class'=>'',
    	'title'=>'',
    	'price'=>'',
    	'period'=>'',
    	'button_text'=>'',
    	'button_link'=>'',
    	'link_target'=>'',
    	'active'=>'',
);

$html = "";

extract(shortcode_atts($args, $atts));

$html .= '<div class="price-table text-center '.$active.'">';
	$html .= '<div class="price-head">';
	$html .= '<h1 class="f-medium normal uppercase">'.$title.'</h1>';
	$html .= '<i class="icon '.$period.' f-big dark "></i>';
	$html .= '</div>';
		$html .= '<div class="price-body">';
		$html .= '<ul>';
		$html .= do_shortcode($content);
		$html .= '</ul>';
		$html .= '</div>';
		$html .= '<div class="separator-center"></div>';
	$html .= '<div class="price-value">';
	$html .= '<h1 class="f-semi-expanded normal uppercase">'.$price.'</h1>';
	$html .= '</div>';
	$html .= '<a href="'.$button_link.'" class="button" target="'.$link_target.'">'.$button_text.'</a>';
$html .= '</div>';

echo $html;