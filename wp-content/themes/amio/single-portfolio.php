<?php $amio_options = get_option('amio'); ?> 
<?php
get_header();
 ?>
<?php if (class_exists('WooCommerce')) { ?>
<ul class="nav navbar-nav f-small woo-nab-ico normal raleway"><li><a href="<?php echo $woocommerce->cart->get_cart_url(); ?>"><i class="ion-bag f-normal"></i></a></li></ul>
<?php } ?>
<?php get_template_part('template-parts/after-menu-section');?>

<!-- Header -->
				<div id="header">
				
					<!-- Section Content -->
					<div class="section-content">
				
						
						<!-- Heading -->
						<div class="big-text" data-uk-scrollspy="{cls:'uk-animation-fade', delay:20}">
						
							<!-- Title -->
							<h2 class="t-left f-large light uppercase"><span class="bold"><?php the_title();?></span></h2>
							
							<!-- Description -->
							<h3 class="t-left f-medium normal uppercase"><?php echo esc_attr(get_post_meta($post->ID,'rnr_portsubttt',true));?></h3>
							
						</div>
						<!-- End Heading -->
						
						
						
					</div>
					<!-- End Section Content -->
					
				</div>
				<!-- End Header -->
		 <div id="about">
		 <!-- Section Content -->
		<div class="section-content">
		 <?php if(get_post_meta($post->ID,'rnr_wr_portdt_pagetype',true)=='st2'){ ?> 
         <?php get_template_part('template-parts/portfolio/style-two');?>
		 <?php } else if(get_post_meta($post->ID,'rnr_wr_portdt_pagetype',true)=='st3'){ ?>
		 <?php get_template_part('template-parts/portfolio/style-three');?>
		 <?php } else if(get_post_meta($post->ID,'rnr_wr_portdt_pagetype',true)=='st4'){ ?>
		 <?php get_template_part('template-parts/portfolio/style-four');?>
		 <?php }
		 else  { ?>
		 <?php get_template_part('template-parts/portfolio/style-one');?>
		 <?php }?>
		 </div>
		 </div>

<?php get_footer(); ?> 