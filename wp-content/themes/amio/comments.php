<?php $amio_options = get_option('amio'); ?> 
<?php

if ( post_password_required() ) {
	return;
}
?>
<div class="clear"></div>
<div id="comments">


<?php if ( have_comments() ) : ?>
	<h3 class="f-normal ultrabold uppercase t-left no-margin"><?php	printf( _n( '%2$s', '%1$s','amio', get_comments_number() ),
									number_format_i18n( get_comments_number() ), '&#8220;' . get_the_title() . '&#8221;' ); ?>  <?php	printf( _n( 'One Comment to', 'Comments: ','amio', get_comments_number() ),
									number_format_i18n( get_comments_number() ), '&#8220;' . get_the_title() . '&#8221;' ); ?>
									</h3>
   
<div class="separator-small"></div>
	

	<div class="wrnavigation">
		<div class="alignleft"><?php previous_comments_link() ?></div>
		<div class="alignright"><?php next_comments_link() ?></div>
	</div>
	<ul class="media-list">
	<?php wp_list_comments('callback=amio_comment');?>
	</ul>

	
 <?php else : // this is displayed if there are no comments so far ?>

	<?php if ( comments_open() ) : ?>
		<!-- If comments are open, but there are no comments. -->

	 <?php else : // comments are closed ?>
		<!-- If comments are closed. -->
		<p class="nocomments"><?php esc_attr_e('Comments are closed.','amio'); ?></p>

	<?php endif; ?>
<?php endif; ?>

<?php if ( comments_open() ) : ?>

<div id="respond" class="comment-respond">
<div class="block-form box-border">



<?php if ( get_option('comment_registration') && !is_user_logged_in() ) : ?>
<h4 class="cm_sub_title"><?php printf(esc_attr__('You must be <a href="%s">logged in</a> to post a comment.','amio'), wp_login_url( get_permalink() )); ?></h4>
<?php else : ?>
<div class="comment-form">
<form action="<?php echo esc_url(site_url()); ?>/wp-comments-post.php" method="post" id="post-comment-form" class="form quform form-overlay  comment-form-wrapper">
<h3 class="f-normal ultrabold uppercase">
<?php if(!empty($amio_options['wr-say'])):?>
<?php echo esc_attr(Amio_AfterSetupTheme::return_thme_option('wr-say',''));?> :
<?php else:?>
<?php esc_attr_e('Say something!','amio');?> :
<?php endif;?>

</h3>
<div class="separator-small"></div>
<br>
<h4 class="section-title"><small><?php cancel_comment_reply_link() ?></small></h4>
<br>

<?php if ( is_user_logged_in() ) : ?>

<h3 class="f-normal ultrabold uppercase"><?php esc_attr_e('Logged in as ','amio'); ?> <?php printf(__('<a href="%1$s">%2$s</a>.','amio'), get_edit_user_link(), $user_identity); ?> <a href="<?php echo esc_url(wp_logout_url(get_permalink())); ?>" title="<?php esc_attr_e('Log out of this account','amio'); ?>"><?php esc_attr_e('Log out &raquo;','amio'); ?></a></h3>
<br>

<?php else : ?>

<div class="row">
<!-- Begin Text input element -->
<div class="col-sm-6">
<?php if(!empty($amio_options['wr-name'])):?>
<input  value="" class="f-small lato" placeholder="
<?php echo esc_attr(Amio_AfterSetupTheme::return_thme_option('wr-name',''));?><?php esc_attr_e('*','amio');?>"  type="text" name="author" id="author"  size="22" tabindex="1" <?php if ($req) echo "aria-required='true'"; ?> />
<?php else: ?>
<input  value="" class="f-small lato" placeholder="
<?php esc_attr_e('Your Name*','amio');?>"  type="text" name="author" id="author"  size="22" tabindex="1" <?php if ($req) echo "aria-required='true'"; ?> />
<?php endif;?>

</div> <!-- end .col-sm-6 -->

<div class="col-sm-6">
<?php if(!empty($amio_options['wr-email'])):?>
<input  value="" type="text" class="f-small lato" placeholder="<?php echo esc_attr(Amio_AfterSetupTheme::return_thme_option('wr-email',''));?><?php esc_attr_e('*','amio');?>" name="email" id="email" size="22" tabindex="2" <?php if ($req) echo "aria-required='true'"; ?> />
<?php else: ?>
<input  value="" type="text" class="f-small lato" placeholder="<?php esc_attr_e('Your Email*','amio');?>" name="email" id="email" size="22" tabindex="2" <?php if ($req) echo "aria-required='true'"; ?> />
<?php endif;?>
</div> <!-- end .col-sm-6 -->
</div>


<?php endif; ?>


<!-- Begin Textarea element -->
<div class="clear"></div>
<div class="row">
<div class="col-lg-12">
<?php if(!empty($amio_options['wr-comment'])):?>
<textarea class="form-control" rows="10" name="comment" id="comment" cols="30"  placeholder="<?php echo esc_attr(Amio_AfterSetupTheme::return_thme_option('wr-comment',''));?><?php esc_attr_e('*','amio');?>" tabindex="4" ></textarea>
<?php else: ?>
<textarea class="form-control" rows="10" name="comment" id="comment" cols="30"  placeholder="<?php esc_attr_e('Your Comment*','amio');?>" tabindex="4" ></textarea>
<?php endif; ?>
</div>
</div>

<!-- End Textarea element -->
<?php if(!empty($amio_options['wr-post'])):?>
<button class="button contact-btn" name="submit" type="submit" id="submit"  tabindex="5" value="" data-uk-scrollspy="{cls:'uk-animation-fade', delay:100}" ><?php echo esc_attr(Amio_AfterSetupTheme::return_thme_option('wr-post',''));?> </button>
<?php else: ?>
<button class="button contact-btn" name="submit" type="submit" id="submit"  tabindex="5" value="" data-uk-scrollspy="{cls:'uk-animation-fade', delay:100}" ><?php esc_attr_e('Post','amio'); ?> </button>
<?php endif; ?>




<?php comment_id_fields(); ?>

<?php do_action('comment_form', $post->ID); ?>


</form>
</div>

<?php endif; // If registration required and not logged in ?>
</div>
</div>

<?php endif; // if you delete this the sky will fall on your head ?>


	

</div><!-- .comments-area -->

