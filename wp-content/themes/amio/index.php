<?php $amio_options = get_option('amio'); ?> 
<?php
get_header();
 ?>
<?php if (class_exists('WooCommerce')) { ?>
<ul class="nav navbar-nav f-small woo-nab-ico normal raleway"><li><a href="<?php echo $woocommerce->cart->get_cart_url(); ?>"><i class="ion-bag f-normal"></i></a></li></ul>
<?php } ?>
<?php get_template_part('template-parts/after-menu-section');?>
 <!-- Header -->
				<div id="header">
				
					<!-- Section Content -->
					<div class="section-content">
				
						
						<!-- Heading -->
						<div class="big-text" data-uk-scrollspy="{cls:'uk-animation-fade', delay:20}">
						
							<!-- Title -->
							<h2 class="t-left f-large ultrabold uppercase">
							<?php if(!empty($amio_options['blogtitle'])):?>
							<?php echo esc_attr(Amio_AfterSetupTheme::return_thme_option('blogtitle',''));?>
							<?php else: ?>
							<?php esc_attr_e('Our Blog','amio');?>
							<?php endif;?>
							</h2>
							
							<!-- Description -->
							<h3 class="t-left f-medium normal uppercase"><?php echo esc_attr(Amio_AfterSetupTheme::return_thme_option('blogsubtitle',''));?></h3>
							
						</div>
						<!-- End Heading -->
						
						
						
					</div>
					<!-- End Section Content -->
					
				</div>
				<!-- End Header -->
 <!-- About -->
<div id="blog">
<!-- Section Content -->
	<div class="section-content">
	
	    <?php if ($amio_options['blog-sidebar']=="st1") {?>
        <?php get_template_part('template-parts/blog/index-left');?>
		<?php } elseif ($amio_options['blog-sidebar']=="st3") {?>
		<?php get_template_part('template-parts/blog/index-full');?>
		<?php } elseif ($amio_options['blog-sidebar']=="st4") {?>
		<?php get_template_part('template-parts/blog/index-four');?>
		<?php } else { ?>
		<?php get_template_part('template-parts/blog/index-right');?>
		<?php } ?>
	    <div style="display:none;"><?php the_tags(); next_posts_link(); previous_posts_link();wp_link_pages();comment_form();paginate_comments_links();previous_comments_link(); wp_enqueue_script('comment-reply'); the_post_thumbnail();?></div>
	</div>		
</div>		
<?php get_footer(); ?>	