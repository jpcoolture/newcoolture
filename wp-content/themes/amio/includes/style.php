<?php
$amio_options = get_option('amio');
function amio_wp_style() {
wp_enqueue_style('amio_main', get_template_directory_uri() . '/style.css');
wp_enqueue_style('amio_plugins', get_template_directory_uri() . '/includes/css/plugins.css');
		
}
add_action('wp_enqueue_scripts', 'amio_wp_style');


	
function amio_enqueue_custom_admin_style() {
wp_register_style( 'custom_wp_admin_css', get_template_directory_uri() . '/includes/css/admin-style.css', false, '1.0.0' );
wp_enqueue_style( 'custom_wp_admin_css' );
}
add_action( 'admin_enqueue_scripts', 'amio_enqueue_custom_admin_style' );	



