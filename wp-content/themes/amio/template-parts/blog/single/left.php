<?php $amio_options = get_option('amio'); ?> 
<!-- Row -->
						<div class="row">
						
							<!-- Column -->
							<div class="col-md-8 pull-right">
							
							<?php if(have_posts()) : while ( have_posts() ) : the_post();?>
								<!-- Blog Post -->
								<div class="blog-post">
								
								
									<?php if( has_post_format( 'video' ) !='') :?>
									<?php get_template_part('template-parts/blog/post-type/video');?>
									<?php elseif( has_post_format( 'gallery' ) !='') :?>
									<?php get_template_part('template-parts/blog/post-type/gallery');?>
									<?php elseif( has_post_format( 'image' ) !='') :?>
									<?php get_template_part('template-parts/blog/post-type/image');?>
									<?php elseif( has_post_format( 'audio' ) !='') :?>
									<?php get_template_part('template-parts/blog/post-type/audio');?>
									<?php else:?>
									<?php endif;?>
									
									
									
									
									
									
									
									<!-- Post Information -->
									
									<ul class="post-info t-left lato f-small">
										<li class="no-margin no-border no-padding">
										<?php if(!empty($amio_options['wr-author'])):?>
										<?php echo esc_attr(Amio_AfterSetupTheme::return_thme_option('wr-author',''));?>
										<?php else:?>
										<?php esc_attr_e('Author: ','amio');?>
										<?php endif;?>
										<a href="<?php the_permalink();?>"><?php the_author();?></a></li>
										<li>
										<?php if(!empty($amio_options['wr-date'])):?>
										<?php echo esc_attr(Amio_AfterSetupTheme::return_thme_option('wr-date',''));?>
										<?php else:?>
										<?php esc_attr_e('Date: ','amio');?>
										<?php endif;?>
										<a href="<?php the_permalink();?>"><?php the_time( get_option( 'date_format' ) ); ?></a></li>
										<li>
										<?php if(!empty($amio_options['wr-in'])):?>
										<?php echo esc_attr(Amio_AfterSetupTheme::return_thme_option('wr-in',''));?>
										<?php else:?>
										<?php esc_attr_e('In: ','amio');?>
										<?php endif;?>
										<?php the_category(', ');?></li>
										<?php
										if( has_tag() ) {?>
										<li><?php the_tags();?></li>
										<?php }?>
									</ul>
									<!-- End Post Information -->
									
									
									
									
									<!-- Post Texts -->
									<div class="post-text wr-unit-test">
									
									
												<?php the_content();
			  
											  wp_link_pages( array(
												'before'      => '<div class="page-links">',
												'after'       => '</div>',
												'link_before' => '<span>',
												'link_after'  => '</span>',
												'pagelink'    => '%',
												'separator'   => '',
											) );
										  ?>
										
										
									</div>
									<!-- End Post Texts -->
									
								</div>
								<!-- End Blog Post -->
								<?php if ( comments_open() || get_comments_number() ) {
								comments_template();
								} ?>
							<?php endwhile;  endif; wp_reset_postdata(); ?>	
							</div>
							<!-- End Column -->
							
							
							
							
							<!-- Column -->
							<div class="col-md-4 pull-left">
							
							<?php
							if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Blog Sidebar')): 
							endif;
							?>
								
								
								
							</div>
							<!-- End Column -->
							
						</div>
						<!-- End Row -->