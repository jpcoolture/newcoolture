<?php
if( !function_exists ('amio_wp_enqueue_scripts') ) :
function amio_wp_scripts() {
global $post, $amio_options;
$amio_options = get_option('amio');
wp_enqueue_script('amio_plugin', get_template_directory_uri() . '/includes/js/plugin.js', array('jquery'), '1.0',true);
wp_enqueue_script('amio_custom', get_template_directory_uri() . '/includes/js/custom.js', array('jquery'), '1.0',true);

		
if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
wp_enqueue_script( 'comment-reply' );
}
		
}
	add_action('wp_enqueue_scripts', 'amio_wp_scripts');
endif;