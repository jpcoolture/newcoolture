<?php $amio_options = get_option('amio'); ?> 
<?php
global $amio_link_target;
if($amio_options['wr-social-target'] == 'self') {
		$amio_link_target = 'target="_self"';
}
else	if($amio_options['wr-social-target'] == 'blank') {
		$amio_link_target = 'target="_blank"';
}
?>
<!-- Footer -->
				<div id="footer">
				
					<!-- Section Content -->
					<div class="section-content">
					
					
					
						<!-- Row -->
						<div class="row">
						
							<!-- Column -->
							<div class="col-md-12" data-uk-scrollspy="{cls:'uk-animation-fade'}">
								<?php if (Amio_AfterSetupTheme::return_thme_option('wr-enable-footer-logo')=='st1'){?>
								<!-- Footer Logo -->
								<div class="footer-logo float-l">
								
									<!-- Image -->
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>" ><img src="<?php echo esc_url(Amio_AfterSetupTheme::return_thme_option('footerlogopic','url'));?>" alt="" /></a>
									<!-- End Image -->
									
								</div>
								<!-- End Footer Logo -->
								<?php } else {?>
								<?php } ?>
								
							</div>
							<!-- End Column -->
							
						</div>
						<!-- End Row -->
						
						
						
						<!-- Row -->
						<div class="row" data-uk-scrollspy="{cls:'uk-animation-fade', delay:20}">
						
						
						
							<!-- Column -->
							<div class="col-md-12 t-left">
							
								<!-- Footer Social -->
								<ul class="footer-social">
									
									<!-- Links -->
						
						<?php if(!empty($amio_options['facebook'])):?>
                        <li><a href="<?php echo esc_url($amio_options['facebook']);?>" target="<?php echo esc_attr($amio_link_target);?>" ><i class="fa fa-facebook dark" aria-hidden="true"></i></a></li>
						<?php endif;?>
						<?php if(!empty($amio_options['twitter'])):?>
                        <li><a href="<?php echo esc_url($amio_options['twitter']);?>" target="<?php echo esc_attr($amio_link_target);?>"><i class="fa fa-twitter dark" aria-hidden="true"></i></a></li>
						<?php endif;?>
						<?php if(!empty($amio_options['instagram'])):?>
                        <li><a href="<?php echo esc_url($amio_options['instagram']);?>" target="<?php echo esc_attr($amio_link_target);?>" ><i class="fa fa-instagram dark" aria-hidden="true"></i></a></li>
						<?php endif;?>
						<?php if(!empty($amio_options['pinterest'])):?>
                        <li><a href="<?php echo esc_url($amio_options['pinterest']);?>" target="<?php echo esc_attr($amio_link_target);?>" ><i class="fa fa-pinterest dark" aria-hidden="true"></i></a></li>
						<?php endif;?>
						<?php if(!empty($amio_options['tumblr'])):?>
                        <li><a href="<?php echo esc_url($amio_options['tumblr']);?>" target="<?php echo esc_attr($amio_link_target);?>" ><i class="fa fa-tumblr dark" aria-hidden="true"></i></a></li>
						<?php endif;?>
						<?php if(!empty($amio_options['gplus'])):?>
                        <li><a href="<?php echo esc_url($amio_options['gplus']);?>" target="<?php echo esc_attr($amio_link_target);?>" ><i class="fa fa-google-plus dark" aria-hidden="true"></i></li>
						<?php endif;?>
						<?php if(!empty($amio_options['linkedin'])):?>
                        <li><a href="<?php echo esc_url($amio_options['linkedin']);?>" target="<?php echo esc_attr($amio_link_target);?>" ><i class="fa fa-linkedin dark" aria-hidden="true"></i></a></li>
						<?php endif;?>
						<?php if(!empty($amio_options['youtube'])):?>
                        <li><a href="<?php echo esc_url($amio_options['youtube']);?>" target="<?php echo esc_attr($amio_link_target);?>" ><i class="fa fa-youtube dark" aria-hidden="true"></i></a></li>
						<?php endif;?>
						
						<?php if(!empty($amio_options['slack'])):?>
                        <li><a href="<?php echo esc_url($amio_options['slack']);?>" target="<?php echo esc_attr($amio_link_target);?>" ><i class="fa fa-slack dark" aria-hidden="true"></i></a></li>
						<?php endif;?>
						<?php if(!empty($amio_options['vimeo'])):?>
                        <li><a href="<?php echo esc_url($amio_options['vimeo']);?>" target="<?php echo esc_attr($amio_link_target);?>" ><i class="fa fa-vimeo dark" aria-hidden="true"></i></a></li>
						<?php endif;?>
												
						<?php if(!empty($amio_options['behance'])):?>
                        <li><a href="<?php echo esc_url($amio_options['behance']);?>" target="<?php echo esc_attr($amio_link_target);?>" ><i class="fa fa-behance dark" aria-hidden="true"></i></a></li>
						<?php endif;?>
						
						<?php if(!empty($amio_options['git'])):?>
                        <li><a href="<?php echo esc_url($amio_options['git']);?>" target="<?php echo esc_attr($amio_link_target);?>" ><i class="fa fa-git dark" aria-hidden="true"></i></a></li>
						<?php endif;?>
						
						<?php if(!empty($amio_options['soundcloud'])):?>
                        <li><a href="<?php echo esc_url($amio_options['soundcloud']);?>" target="<?php echo esc_attr($amio_link_target);?>" ><i class="fa fa-soundcloud dark" aria-hidden="true"></i></a></li>
						<?php endif;?>
						
						<?php if(!empty($amio_options['dribbble'])):?>
                        <li><a href="<?php echo esc_url($amio_options['dribbble']);?>" target="<?php echo esc_attr($amio_link_target);?>" ><i class="fa fa-dribbble dark" aria-hidden="true"></i></a></li>
						<?php endif;?>
						
						<?php if(!empty($amio_options['xing'])):?>
                        <li><a href="<?php echo esc_url($amio_options['xing']);?>" target="<?php echo esc_attr($amio_link_target);?>" ><i class="fa fa-xing dark" aria-hidden="true"></i></a></li>
						<?php endif;?>
									<!-- End Links -->
									
								</ul>
								<!-- End Footer Social -->
								
								<!-- Separator -->
								<div class="separator-small"></div>
								<!-- End Separator -->
								
							</div>
							<!-- End Column -->
							
							
							
							<!-- Column -->
							<div class="col-md-12" data-uk-scrollspy="{cls:'uk-animation-fade', delay:40}">
							
								<!-- Footer Details -->
								<div class="footer-details t-left">
								
									<?php $amio_copy = Amio_AfterSetupTheme::return_thme_option('copyright');?>
									<?php if($amio_copy != '') {?>
									<?php echo apply_filters('the_content',$amio_copy);?>
									<?php } else { ?>
									<p class="dark f-small lato"><?php esc_attr_e('&copy; 2016 AMIO. ALL RIGHTS RESERVED','amio');?></p>
									<?php } ;?>
								
								</div>
								<!-- End Footer Details -->
								
							</div>
							<!-- End Column -->
							
							
						</div>
						<!-- End Row -->
						
					</div>
					<!-- End Section Content -->
					
				</div>
				<!-- End Footer -->
				
				
				
				
				
				
			</div>
			<!-- End All Sections -->	
</div>
<?php wp_footer(); ?>
</body>
</html>