<?php $amio_options = get_option('amio'); ?> 
<div class="row">
<!-- Column -->
<div class="col-md-6 proj">
								<?php $amio_images = rwmb_meta( 'rnr_portfolio-image','type=image&size=' );
                                    foreach ( $amio_images as $amio_image ){
                                        echo "<a href='{$amio_image['url']}' data-lightbox='works' title=''><img src='{$amio_image['url']}' alt=''></a>
							   ";
								 };?>
								
</div>
<!-- End Column -->
<!-- Column -->
							<div class="col-md-6">
							
								<div class="col-md-12">
								
									<!-- Brief -->
									<div class="brief no-margin text-left">
									<?php if(have_posts()) : while ( have_posts() ) : the_post();?>
									<?php the_content();?>
									<?php endwhile;  endif; wp_reset_postdata(); ?>
										

										
									</div>
									<!-- End Brief -->
									
								</div>
								<!-- End Column -->
								
							</div>
							<!-- End Column -->
</div>